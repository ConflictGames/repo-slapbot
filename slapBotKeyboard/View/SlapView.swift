//
//  SlapView.swift
//  Slapbot
//
//  Created by Nikul Dholiya on 20/01/18.
//  Copyright © 2018 Khatri Dhruv. All rights reserved.
//

import UIKit

class SlapView: UIView {

    @IBOutlet weak var collectionSlap: UICollectionView!
    @IBOutlet weak var tblSlap: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var keyboardView: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblQ: UILabel!
    @IBOutlet weak var lblW: UILabel!
    @IBOutlet weak var lblE: UILabel!
    @IBOutlet weak var lblR: UILabel!
    @IBOutlet weak var lblT: UILabel!
    @IBOutlet weak var lblY: UILabel!
    @IBOutlet weak var lblU: UILabel!
    @IBOutlet weak var lblI: UILabel!
    @IBOutlet weak var lblO: UILabel!
    @IBOutlet weak var lblP: UILabel!
    @IBOutlet weak var lblA: UILabel!
    @IBOutlet weak var lblS: UILabel!
    @IBOutlet weak var lblD: UILabel!
    @IBOutlet weak var lblF: UILabel!
    @IBOutlet weak var lblG: UILabel!
    @IBOutlet weak var lblH: UILabel!
    @IBOutlet weak var lblJ: UILabel!
    @IBOutlet weak var lblK: UILabel!
    @IBOutlet weak var lblL: UILabel!
    @IBOutlet weak var lblZ: UILabel!
    @IBOutlet weak var lblX: UILabel!
    @IBOutlet weak var lblC: UILabel!
    @IBOutlet weak var lblV: UILabel!
    @IBOutlet weak var lblB: UILabel!
    @IBOutlet weak var lblN: UILabel!
    @IBOutlet weak var lblM: UILabel!
    @IBOutlet weak var lblBack: UILabel!
    @IBOutlet weak var lblEnter: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblCaps: UILabel!
    @IBOutlet weak var lblSpace: UILabel!
    @IBOutlet weak var lblChangeKeyboard: UILabel!

    @IBOutlet weak var btnPurchase: UIButton!
    @IBOutlet weak var viewPurchase: UIView!
    @IBOutlet weak var btnShowKeyboard: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var viewBackSpace: UIView!
    @IBOutlet weak var viewCapsLock: UIView!
    @IBOutlet weak var viewSpace: UIView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewHeart: UIView!
    @IBOutlet weak var btnKeyboardForOriginal: UIButton!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var viewPermission: UIView!
    
    @IBOutlet weak var btnBackSpace: UIButton!
    @IBOutlet weak var btnSettings: UIButton!
    
    @IBOutlet weak var viewInternet: UIView!
    @IBOutlet weak var btnCloseNoInternet: UIButton!
    @IBOutlet weak var btnChangeKeyboard2: UIButton!
    @IBOutlet weak var progressActivity: UIActivityIndicatorView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
