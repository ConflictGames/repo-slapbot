//
//  KeyboardController.swift
//  slapBotKeyboard
//
//  Created by dhruv Khatri on 14/02/18.
//  Copyright © 2018 Khatri Dhruv. All rights reserved.
//

import UIKit
import Alamofire
import SwipeCellKit
import SDWebImage

class KeyboardController: UIInputViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SwipeTableViewCellDelegate   {
    var slapView: SlapView!
    
    var arrSlap = ["Slaps","Evil eyes","tongue twister","Your Moms","Insults","Favorites", "Recent"]
    var arrtblDesc = ["it never calculate the true means joy  until its robot palm collied  with face","slapped you hard  the terminator  just  gave me  his job","it will slap harder then new prisoners hold soap"]
    var arrSlapDetail = NSMutableArray()
    var arrProductList = NSMutableArray()
    var heightConstraint: NSLayoutConstraint!
    var widthConstraint : NSLayoutConstraint!
    var height1 : CGFloat = 290
    var height2 : CGFloat = 260
    var selectedProduct = 0
    var preProduct = 0
    var currentIndex = 0
    var fav = [Int]()
    var isBackSpace = false
    var dic = ["Slaps" : "slap",
               "Hairline Jokes" : "hairline",
               "Appreciations" : "appreciate",
               "I Love You More Than" : "love",
               "Tongue Twisters" : "Twister"]
    
  
    
 
    var capsOn = ["Q","W","E","R","T","Y","U","I","O","P","A","S","D","F","G","H","J","K","L","Z","X","C","V","B","N","M"]
    var capsOf = ["q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m"]
    var num1 = ["1","2","3","4","5","6","7","8","9","0","-","/",":",";","(",")","$","&","@","~",".",",","?","!","'",".com"]
    var num2 = ["[","]","{","}","#","%","^","*","+","=","_","\\","|","~","<",">","$","£","€",".",",","?","!","'","~","+"]
    
    var lblArray = [UILabel]()
    var isCaps = true
    var isNum = false
    var isKeyboardVisible = false
    var isOriginal = false
    var txtWord = ""
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        // Add custom view sizing constraints here
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.slapView.progressActivity.isHidden = false
        self.slapView.progressActivity.startAnimating()
        let timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (true) in
            self.removeStart()
        }
     //   print(UserDefaults.standard.value(forKey: "Love"))
        
        
    //    UserDefaults.standard.set("hello", forKey: "Google")
        
        
        
        if let arrFav = UserDefaults.standard.value(forKey: "Fav") as? [Int]
        {
            self.fav = arrFav
        }
        
        
        
      
        if #available(iOS 11.0, *) {
            self.heightConstraint = NSLayoutConstraint( item:self.inputView as Any, attribute:.height, relatedBy:.equal, toItem:nil, attribute:.notAnAttribute, multiplier:0.0, constant:height1)
            self.view.addConstraint(self.heightConstraint)
            self.view.translatesAutoresizingMaskIntoConstraints = false
            
            self.widthConstraint = NSLayoutConstraint( item:self.inputView as Any, attribute:.width, relatedBy:.equal, toItem:nil, attribute:.notAnAttribute, multiplier:0.0, constant:UIScreen.main.bounds.width)
            self.view.addConstraint(self.widthConstraint)
            self.view.translatesAutoresizingMaskIntoConstraints = false
            self.slapView = Bundle.main.loadNibNamed("SlapView", owner: self, options: nil)![0] as! SlapView
            self.slapView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 280)
            self.slapView.tblSlap.delegate = self
            self.slapView.tblSlap.dataSource = self
            self.slapView.tblSlap.showsVerticalScrollIndicator = false
            self.slapView.tblSlap.separatorStyle = .none
            
            self.slapView.collectionSlap.delegate = self
            self.slapView.collectionSlap.dataSource = self
            self.slapView.collectionSlap.showsHorizontalScrollIndicator = false
            
            self.slapView.collectionSlap.register(UINib(nibName: "SlapCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SlapCollectionCell")
            self.slapView.tblSlap.register(UINib(nibName: "SlapTableCell", bundle: nil), forCellReuseIdentifier: "SlapTableCell")
            view.addSubview(slapView)
            self.setCornerRadius()
            self.setupKeyboard()
            self.getProductList()
            getSlapDetail(product: "slap", isSearch: false, searchString: "")
            self.slapView.keyboardView.isHidden = true
            self.slapView.viewPurchase.isHidden = true
            self.slapView.viewInternet.isHidden = true
            if(isOpenAccessGranted())
            {
                self.slapView.viewPermission.isHidden = true
            }
            else
            {
                self.slapView.viewPermission.isHidden = false
            }
        }
            
        else
        {
           
            self.slapView = Bundle.main.loadNibNamed("SlapView2", owner: self, options: nil)![0] as! SlapView
            self.slapView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 280)
            self.slapView.tblSlap.delegate = self
            self.slapView.tblSlap.dataSource = self
            self.slapView.tblSlap.showsVerticalScrollIndicator = false
            self.slapView.tblSlap.separatorStyle = .none
            
            self.slapView.collectionSlap.delegate = self
            self.slapView.collectionSlap.dataSource = self
            self.slapView.collectionSlap.showsHorizontalScrollIndicator = false
            
            self.slapView.collectionSlap.register(UINib(nibName: "SlapCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SlapCollectionCell")
            self.slapView.tblSlap.register(UINib(nibName: "SlapTableCell", bundle: nil), forCellReuseIdentifier: "SlapTableCell")
            view.addSubview(slapView)
            self.setCornerRadius()
            self.setupKeyboard()
            self.getProductList()
            getSlapDetail(product: "slap", isSearch: false, searchString: "")
            self.slapView.keyboardView.isHidden = true
            self.slapView.viewPurchase.isHidden = true
            self.slapView.viewInternet.isHidden = true
            if(isOpenAccessGranted())
            {
                self.slapView.viewPermission.isHidden = true
            }
            else
            {
                self.slapView.viewPermission.isHidden = false
            }
            
            
            
      
//            self.heightConstraint = NSLayoutConstraint( item:self.inputView as Any, attribute:.height, relatedBy:.equal, toItem:nil, attribute:.notAnAttribute, multiplier:0.0, constant:height2)
//            self.view.addConstraint(self.heightConstraint)
//            self.view.translatesAutoresizingMaskIntoConstraints = false
//
//            self.widthConstraint = NSLayoutConstraint( item:self.inputView as Any, attribute:.width, relatedBy:.equal, toItem:nil, attribute:.notAnAttribute, multiplier:0.0, constant:UIScreen.main.bounds.width)
//            self.view.addConstraint(self.widthConstraint)
//            self.view.translatesAutoresizingMaskIntoConstraints = false
//            self.view.setNeedsUpdateConstraints()
//            self.slapView = Bundle.main.loadNibNamed("SlapView", owner: self, options: nil)![0] as! SlapView
//            self.slapView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 260.0)
//            self.slapView.tblSlap.delegate = self
//            self.slapView.tblSlap.dataSource = self
//            self.slapView.tblSlap.showsVerticalScrollIndicator = false
//            self.slapView.tblSlap.separatorStyle = .none
//
//            self.slapView.collectionSlap.delegate = self
//            self.slapView.collectionSlap.dataSource = self
//            self.slapView.collectionSlap.showsHorizontalScrollIndicator = false
//
//            self.slapView.collectionSlap.register(UINib(nibName: "SlapCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SlapCollectionCell")
//            self.slapView.tblSlap.register(UINib(nibName: "SlapTableCell", bundle: nil), forCellReuseIdentifier: "SlapTableCell")
//            view.addSubview(slapView)
//            self.setCornerRadius()
//            self.setupKeyboard()
//            self.getProductList()
//            getSlapDetail(product: "slap", isSearch: false, searchString: "")
            //            self.slapView = Bundle.main.loadNibNamed("SlapView", owner: self, options: nil)![0] as! SlapView
            //            self.slapView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216.0)
        }
        
       
        
        //   Perform custom UI setup here
        //        self.nextKeyboardButton = UIButton(type: .system)
        //
        //        self.nextKeyboardButton.setTitle(NSLocalizedString("Next Keyboard", comment: "Title for 'Next Keyboard' button"), for: [])
        //        self.nextKeyboardButton.sizeToFit()
        //        self.nextKeyboardButton.translatesAutoresizingMaskIntoConstraints = false
        //
        //        self.nextKeyboardButton.addTarget(self, action: #selector(handleInputModeList(from:with:)), for: .allTouchEvents)
        //
        //        self.view.addSubview(self.nextKeyboardButton)
        //
        //        self.nextKeyboardButton.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        //        self.nextKeyboardButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        
        
        self.slapView.viewInternet.isHidden = false

        if (!NetworkReachabilityManager()!.isReachable)
        {
            self.slapView.viewInternet.isHidden = false
        }
    }
    
   
    
    func removeStart()
    {
        if(isBackSpace)
        {
            if(isOriginal)
            {
                (textDocumentProxy as UIKeyInput).deleteBackward()
            }
            else
            {
                if(self.slapView.txtSearch.text != "")
                {
                    self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!.dropLast())"
                }
            }
            txtWord = String(txtWord.dropLast())
            checkWords()
            print("Space")
//            (textDocumentProxy as UIKeyInput).deleteBackward()
//            txtWord = String(txtWord.dropLast())
        }
    }
    func isOpenAccessGranted() -> Bool{
        UIPasteboard.general.image = #imageLiteral(resourceName: "ic_meh")
        return UIPasteboard.general.hasImages
    }
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 11.0, *) {
            
            
        }
            
        else
        {
//            self.heightConstraint = NSLayoutConstraint( item:self.inputView as Any, attribute:.height, relatedBy:.equal, toItem:nil, attribute:.notAnAttribute, multiplier:0.0, constant:290)
//            self.view.addConstraint(self.heightConstraint)
//            self.view.translatesAutoresizingMaskIntoConstraints = false
//
//            self.widthConstraint = NSLayoutConstraint( item:self.view as Any, attribute:.width, relatedBy:.equal, toItem:nil, attribute:.notAnAttribute, multiplier:0.0, constant:UIScreen.main.bounds.width)
//            self.view.addConstraint(self.widthConstraint)
//            self.view.translatesAutoresizingMaskIntoConstraints = false
           
            //  let heightConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: 290)
            //      self.view.addConstraint(heightConstraint)
            
            
            
            
            //            self.slapView = Bundle.main.loadNibNamed("SlapView", owner: self, options: nil)![0] as! SlapView
            //            self.slapView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216.0)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }
    
    override func textWillChange(_ textInput: UITextInput?) {
        // The app is about to change the document's contents. Perform any preparation here.
    }
    
    override func textDidChange(_ textInput: UITextInput?) {
        // The app has just changed the document's contents, the document context has been updated.
        
        //        var textColor: UIColor
        //        let proxy = self.textDocumentProxy
        //        if proxy.keyboardAppearance == UIKeyboardAppearance.dark {
        //            textColor = UIColor.white
        //        } else {
        //            textColor = UIColor.black
        //        }
        //        self.nextKeyboardButton.setTitleColor(textColor, for: [])
    }
    
    func setCornerRadius() {
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblA, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblB, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblC, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblD, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblE, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblF, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblG, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblH, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblI, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblJ, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblK, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblL, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblM, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblN, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblO, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblP, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblQ, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblR, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblS, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblT, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblU, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblV, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblW, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblX, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblY, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblZ, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblChangeKeyboard, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.viewSpace, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.viewSearch, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.viewCapsLock, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.viewBackSpace, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.viewHeart, radius: 5.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.btnPurchase, radius: 5.0)
    }
    
    func setupKeyboard() {
        
        let tapA = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblA))
        self.slapView.lblA.isUserInteractionEnabled = true
        self.slapView.lblA.addGestureRecognizer(tapA)
        
        let tapB = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblB))
        self.slapView.lblB.isUserInteractionEnabled = true
        self.slapView.lblB.addGestureRecognizer(tapB)
        
        let tapC = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblC))
        self.slapView.lblC.isUserInteractionEnabled = true
        self.slapView.lblC.addGestureRecognizer(tapC)
        
        let tapD = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblD))
        self.slapView.lblD.isUserInteractionEnabled = true
        self.slapView.lblD.addGestureRecognizer(tapD)
        
        let tapE = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblE))
        self.slapView.lblE.isUserInteractionEnabled = true
        self.slapView.lblE.addGestureRecognizer(tapE)
        
        let tapF = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblF))
        self.slapView.lblF.isUserInteractionEnabled = true
        self.slapView.lblF.addGestureRecognizer(tapF)
        
        let tapG = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblG))
        self.slapView.lblG.isUserInteractionEnabled = true
        self.slapView.lblG.addGestureRecognizer(tapG)
        
        let tapH = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblH))
        self.slapView.lblH.isUserInteractionEnabled = true
        self.slapView.lblH.addGestureRecognizer(tapH)
        
        let tapI = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblI))
        self.slapView.lblI.isUserInteractionEnabled = true
        self.slapView.lblI.addGestureRecognizer(tapI)
        
        let tapJ = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblJ))
        self.slapView.lblJ.isUserInteractionEnabled = true
        self.slapView.lblJ.addGestureRecognizer(tapJ)
        
        let tapK = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblK))
        self.slapView.lblK.isUserInteractionEnabled = true
        self.slapView.lblK.addGestureRecognizer(tapK)
        
        let tapL = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblL))
        self.slapView.lblL.isUserInteractionEnabled = true
        self.slapView.lblL.addGestureRecognizer(tapL)
        
        let tapM = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblM))
        self.slapView.lblM.isUserInteractionEnabled = true
        self.slapView.lblM.addGestureRecognizer(tapM)
        
        let tapN = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblN))
        self.slapView.lblN.isUserInteractionEnabled = true
        self.slapView.lblN.addGestureRecognizer(tapN)
        
        let tapO = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblO))
        self.slapView.lblO.isUserInteractionEnabled = true
        self.slapView.lblO.addGestureRecognizer(tapO)
        
        let tapP = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblP))
        self.slapView.lblP.isUserInteractionEnabled = true
        self.slapView.lblP.addGestureRecognizer(tapP)
        
        let tapQ = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblQ))
        self.slapView.lblQ.isUserInteractionEnabled = true
        self.slapView.lblQ.addGestureRecognizer(tapQ)
        
        let tapR = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblR))
        self.slapView.lblR.isUserInteractionEnabled = true
        self.slapView.lblR.addGestureRecognizer(tapR)
        
        let tapS = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblS))
        self.slapView.lblS.isUserInteractionEnabled = true
        self.slapView.lblS.addGestureRecognizer(tapS)
        
        let tapT = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblT))
        self.slapView.lblT.isUserInteractionEnabled = true
        self.slapView.lblT.addGestureRecognizer(tapT)
        
        let tapU = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblU))
        self.slapView.lblU.isUserInteractionEnabled = true
        self.slapView.lblU.addGestureRecognizer(tapU)
        
        let tapV = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblV))
        self.slapView.lblV.isUserInteractionEnabled = true
        self.slapView.lblV.addGestureRecognizer(tapV)
        
        let tapW = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblW))
        self.slapView.lblW.isUserInteractionEnabled = true
        self.slapView.lblW.addGestureRecognizer(tapW)
        
        let tapX = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblX))
        self.slapView.lblX.isUserInteractionEnabled = true
        self.slapView.lblX.addGestureRecognizer(tapX)
        
        let tapY = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblY))
        self.slapView.lblY.isUserInteractionEnabled = true
        self.slapView.lblY.addGestureRecognizer(tapY)
        
        let tapZ = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblZ))
        self.slapView.lblZ.isUserInteractionEnabled = true
        self.slapView.lblZ.addGestureRecognizer(tapZ)
        
        let tapCaps = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblCaps))
        self.slapView.lblCaps.isUserInteractionEnabled = true
        self.slapView.lblCaps.addGestureRecognizer(tapCaps)
        
        let tapNum = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblNum))
        self.slapView.lblNumber.isUserInteractionEnabled = true
        self.slapView.lblNumber.addGestureRecognizer(tapNum)
        
        let tapSpace = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblSpace))
        self.slapView.lblSpace.isUserInteractionEnabled = true
        self.slapView.lblSpace.addGestureRecognizer(tapSpace)
        
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblBackSpace))
        self.slapView.lblBack.isUserInteractionEnabled = true
        self.slapView.lblBack.addGestureRecognizer(tapBack)
        
        let tapLongBack = UILongPressGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblBackSpace))
        self.slapView.lblBack.isUserInteractionEnabled = true
        self.slapView.lblBack.addGestureRecognizer(tapLongBack)
        
        let tapChangeKeyBoard = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.tapLblChangeKeyboard))
        self.slapView.lblChangeKeyboard.isUserInteractionEnabled = true
        self.slapView.lblChangeKeyboard.addGestureRecognizer(tapChangeKeyBoard)
        
        let tapSearch = UITapGestureRecognizer(target: self, action: #selector(KeyboardController.searchButton))
        self.slapView.lblEnter.isUserInteractionEnabled = true
        self.slapView.lblEnter.addGestureRecognizer(tapSearch)
        
        
        self.slapView.btnSearch.addTarget(self, action: #selector(searchButton), for: .touchUpInside)
        self.slapView.btnPurchase.addTarget(self, action: #selector(hidePurchaseClicked), for: .touchUpInside)
        self.slapView.btnShowKeyboard.addTarget(self, action: #selector(showKeyboard), for: .touchUpInside)
        self.slapView.btnKeyboardForOriginal.addTarget(self, action: #selector(originalKeyboradPressed(sender:)), for: .touchUpInside)
        self.slapView.btnBack.addTarget(self, action: #selector(backClicked(sender:)), for: .touchUpInside)
        self.slapView.btnBack.addTarget(self, action: #selector(backClicked(sender:)), for: .touchDown)
        self.slapView.btnBackSpace.addTarget(self, action: #selector(tapLblBackSpace), for: .touchDown)
        self.slapView.btnBackSpace.addTarget(self, action: #selector(tapUpBackSpace), for: .touchUpInside)
        self.slapView.btnChangeKeyboard2.addTarget(self, action: #selector(tapLblChangeKeyboard), for: .touchUpInside)
        
        self.slapView.btnCloseNoInternet.addTarget(self, action: #selector(hideInternetError), for: .touchUpInside)

      //  "q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m"
        
        self.lblArray.append(self.slapView.lblQ)
        self.lblArray.append(self.slapView.lblW)
        self.lblArray.append(self.slapView.lblE)
        self.lblArray.append(self.slapView.lblR)
        self.lblArray.append(self.slapView.lblT)
        self.lblArray.append(self.slapView.lblY)
        self.lblArray.append(self.slapView.lblU)
        self.lblArray.append(self.slapView.lblI)
        self.lblArray.append(self.slapView.lblO)
        self.lblArray.append(self.slapView.lblP)
        self.lblArray.append(self.slapView.lblA)
        self.lblArray.append(self.slapView.lblS)
        self.lblArray.append(self.slapView.lblD)
        self.lblArray.append(self.slapView.lblF)
        self.lblArray.append(self.slapView.lblG)
        self.lblArray.append(self.slapView.lblH)
        self.lblArray.append(self.slapView.lblJ)
        self.lblArray.append(self.slapView.lblK)
        self.lblArray.append(self.slapView.lblL)
        self.lblArray.append(self.slapView.lblZ)
        self.lblArray.append(self.slapView.lblX)
        self.lblArray.append(self.slapView.lblC)
        self.lblArray.append(self.slapView.lblV)
        self.lblArray.append(self.slapView.lblB)
        self.lblArray.append(self.slapView.lblN)
        self.lblArray.append(self.slapView.lblM)
        changeKeyboard(index : 1)
        
    }
    
    //MARK:- Button Events
    
    @objc func backClicked(sender : UIButton)
    {
        self.slapView.viewPurchase.isHidden = true
        getSlapDetail(product: "\((dic[(arrProductList[0] as! SlapProductModel).productName])!)", isSearch: false, searchString: "")
    }
    
    
    
    @objc func showKeyboard()
    {
        isOriginal = false
        self.slapView.keyboardView.isHidden = false
    }
    
    func hideKeyboard()
    {
        self.slapView.keyboardView.isHidden = true
    }
    @objc func searchButton()
    {
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\n")
           // self.slapView.keyboardView.isHidden = true
        }
        else
        {
        getSlapDetail(product: "\((dic[(arrProductList[selectedProduct] as! SlapProductModel).productName])!)", isSearch: true, searchString: "\(self.slapView.txtSearch.text!)")
        self.slapView.keyboardView.isHidden = true
        }
    }
    
    @objc func purchaseClicked()
    {
        self.slapView.keyboardView.isHidden = true
        self.slapView.viewPurchase.isHidden = false
    }
    
    @objc func openURL(_ url: URL) {
        return
    }
    
    func openApp(_ urlstring:String) {
        
        var responder: UIResponder? = self as UIResponder
        let selector = #selector(openURL(_:))
        while responder != nil {
            if responder!.responds(to: selector) && responder != self {
                responder!.perform(selector, with: URL(string: urlstring)!)
                return
            }
            responder = responder?.next
        }
    }
    
    
    @objc func hidePurchaseClicked()
    {
        
        self.openApp("purchase://")
        self.slapView.keyboardView.isHidden = true
        self.slapView.viewPurchase.isHidden = true
    }
    
    //MARK:- UITapGestureRecognizer Click
    @objc func tapLblA(sender: UITapGestureRecognizer) {
        print("A")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)A"
        checkWords()
    }
    
    @objc func tapLblB(sender: UITapGestureRecognizer) {
        print("B")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)B"
        checkWords()
    }
    
    @objc func tapLblC(sender: UITapGestureRecognizer) {
        print("C")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)C"
        checkWords()
    }
    
    @objc func tapLblD(sender: UITapGestureRecognizer) {
        print("D")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblE(sender: UITapGestureRecognizer) {
        print("E")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblF(sender: UITapGestureRecognizer) {
        print("F")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblG(sender: UITapGestureRecognizer) {
        print("G")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblH(sender: UITapGestureRecognizer) {
        print("H")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblI(sender: UITapGestureRecognizer) {
        print("I")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblJ(sender: UITapGestureRecognizer) {
        print("J")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblK(sender: UITapGestureRecognizer) {
        print("K")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblL(sender: UITapGestureRecognizer) {
        print("L")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblM(sender: UITapGestureRecognizer) {
        print("M")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblN(sender: UITapGestureRecognizer) {
        print("N")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblO(sender: UITapGestureRecognizer) {
        print("O")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblP(sender: UITapGestureRecognizer) {
        print("P")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblQ(sender: UITapGestureRecognizer) {
        print("Q")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblR(sender: UITapGestureRecognizer) {
        print("R")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblS(sender: UITapGestureRecognizer) {
        print("S")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblT(sender: UITapGestureRecognizer) {
        print("T")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblU(sender: UITapGestureRecognizer) {
        print("U")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblV(sender: UITapGestureRecognizer) {
        print("V")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblW(sender: UITapGestureRecognizer) {
        print("W")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblX(sender: UITapGestureRecognizer) {
        print("X")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblY(sender: UITapGestureRecognizer) {
        print("Y")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblZ(sender: UITapGestureRecognizer) {
        print("Z")
        if(isOriginal)
        {
            (textDocumentProxy as UIKeyInput).insertText("\(((sender.view as? UILabel)?.text)!)")
        }
        else
        {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!)\(((sender.view as? UILabel)?.text)!)"
        }
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    
    func checkWords()
    {
        if(txtWord != "" && !isNum)
        {
            changeKeyboard(index: 1)
        }
        else if(txtWord == "" && !isNum)
        {
            changeKeyboard(index: 0)
        }
    }
    
    
    @objc func tapLblCaps() {
        if(isNum)
        {
            if(isCaps)
            {
                //1
                changeKeyboard(index: 3)
            }
            else
            {
                //0
                changeKeyboard(index: 2)
            }
        }
        else
        {
        if(isCaps)
        {
            //1
            changeKeyboard(index: 1)
        }
        else
        {
            //0
            changeKeyboard(index: 0)
        }
        }
        isCaps = !isCaps
        //isNum = true
        print("Z")
    }
    
    @objc func tapLblSpace() {if(isOriginal)
    {
        (textDocumentProxy as UIKeyInput).insertText(" ")
        
    }
    else
    {
        self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!) "
        }
        print("Z")
        txtWord = "\(txtWord)D"
        checkWords()
    }
    
    @objc func tapLblNum() {
        print("Z")
        if(isNum)
        {
            //3
            changeKeyboard(index: 0)
        }
        else
        {
            //2
            changeKeyboard(index: 2)

        }
        isNum = !isNum
    }
    
    @objc func tapLblChangeKeyboard() {
        print("Z")
        advanceToNextInputMode()
    }
    
    @objc func hideInternetError()
    {
        self.slapView.keyboardView.isHidden = false
        self.slapView.viewInternet.isHidden = true
    }
    
    @objc func tapLblBackSpace() {
        isBackSpace = true
//        if(isOriginal)
//        {
//            (textDocumentProxy as UIKeyInput).deleteBackward()
//        }
//        else
//        {
//        if(self.slapView.txtSearch.text != "")
//        {
//            self.slapView.txtSearch.text = "\(self.slapView.txtSearch.text!.dropLast())"
//        }
//        }
//        txtWord = String(txtWord.dropLast())
//        checkWords()
//        print("Space")
    }
    @objc func tapUpBackSpace() {
        isBackSpace = false
    }
    
    @objc func tapSettings() {
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        openURL(settingsUrl)
    }
    
    func openUrl(url: URL?) {
        let selector = sel_registerName("openURL:")
        var responder = self as UIResponder?
        while let r = responder, !r.responds(to: selector) {
            responder = r.next
        }
        _ = responder?.perform(selector, with: url)
    }
    
    
    @objc func originalKeyboradPressed(sender : UIButton)
    {
        isOriginal = true

        if(isKeyboardVisible)
        {
            self.slapView.keyboardView.isHidden = true
            sender.setImage(#imageLiteral(resourceName: "keyboard"), for: .normal)
        }
        else
        {
            self.slapView.keyboardView.isHidden = false
            sender.setImage(#imageLiteral(resourceName: "listboard (1)"), for: .normal)
        }
        isKeyboardVisible = !isKeyboardVisible
    }
    
    //MARK Keyboard Setup
    func changeKeyboard(index : Int)
    {
        switch index {
        case 0://Capslock On
            var count = 0
            for lbl in lblArray
            {
                lbl.text = capsOn[count]
                count = count + 1
            }
            break
        case 1://Capslock Off
            var count = 0
            for lbl in lblArray
            {
                lbl.text = capsOf[count]
                count = count + 1
            }
            break
        case 2://Number 1
            var count = 0
            for lbl in lblArray
            {
                lbl.text = num1[count]
                count = count + 1
            }
            break
        case 3://Number 2
            var count = 0
            for lbl in lblArray
            {
                lbl.text = num2[count]
                count = count + 1
            }
            break
        default:
            break
        }
    }
    
    
    
    //    //MARK:- API Call
    func getProductList() {
        
        let URL = "http://conflictapi.com/v1/api/productlist?app_id=ac63d082&app_key=0fd23f82553331a53caa5b497708e8b3&allowedpackages=slapbot&activeonly=false&maxrecords=50"
        
        //    SPAppData.sharedInstance.showLoader()
        SPAPIUtilities.sharedInstance.GetAPICallWith(url: URL) { (response, error) in
            //SPAppData.sharedInstance.dismissLoader()
            if error == nil {
                self.arrProductList.removeAllObjects()
                if let arrResponse = response as? NSArray{
                    for dic in arrResponse {
                        let model = SlapProductModel(attributeDict: dic as! NSDictionary)
                        self.arrProductList.add(model)
                    }
                    
                  //  DispatchQueue.main.async {
                        self.slapView.collectionSlap.reloadData()
                  //  }
                    
                }
            } else {
                print(error)
                // SPAppData.sharedInstance.showAlert(title: "", message: "Something went wrong", viewController: self)
            }
            
        }
        
    }
    
    func getSlapDetail(product : String, isSearch : Bool , searchString : String) {
        
        var URL : String = ""
        if(isSearch)
        {
            URL = "http://conflictapi.com/v1/api/\(product)?app_id=ac63d082&app_key=0fd23f82553331a53caa5b497708e8b3&allowedpackages=slapbot&ActiveOnly=false&maxrecords=50&q=\(searchString)"
        }
        else
        {
            URL = "http://conflictapi.com/v1/api/\(product)?app_id=ac63d082&app_key=0fd23f82553331a53caa5b497708e8b3&allowedpackages=slapbot&ActiveOnly=false&maxrecords=50"
            
        }
        
        SPAPIUtilities.sharedInstance.GetAPICallWith(url: URL) { (response, error) in
            SPAppData.sharedInstance.dismissLoader()
            if error == nil {
                self.slapView.progressActivity.isHidden = true
                self.arrSlapDetail.removeAllObjects()
                if let arrResponse = response as? NSArray{
                    for dic in arrResponse {
                        let model = SlapDetailModel(attributeDict: dic as! NSDictionary)
                        self.arrSlapDetail.add(model)
                    }
                    
                    DispatchQueue.main.async {
                        self.slapView.tblSlap.reloadData()
                    }
                    
                }
                
            } else {
                self.slapView.progressActivity.isHidden = true
                print(error)
                //   SPAppData.sharedInstance.showAlert(title: "", message: "Something went wrong", viewController: self)
            }
            
        }
    }
    
   
    
    
    
    
    func getFav()
    {
        var str = ""
        if(self.fav.count > 0)
        {
        str = "\(self.fav[0])"
        for id in self.fav
        {
            str = str + ",\(id)"
        }
        }
        // http://conflictapi.com/v1/api/productlist?app_id=ac63d082&app_key=0fd23f82553331a53caa5b497708e8b3&allowedpackages=slapbot&activeonly=false&maxrecords=50
        
        let URL = "http://conflictapi.com/v1/api/slap?app_id=ac63d082&app_key=0fd23f82553331a53caa5b497708e8b3&allowedpackages=slapbot&ActiveOnly=false&id=\(str)&maxrecords=50"
        
        SPAPIUtilities.sharedInstance.GetAPICallWith(url: URL) { (response, error) in
            SPAppData.sharedInstance.dismissLoader()
            if error == nil {
                self.arrSlapDetail.removeAllObjects()
                if let arrResponse = response as? NSArray{
                    for dic in arrResponse {
                        let model = SlapDetailModel(attributeDict: dic as! NSDictionary)
                        self.arrSlapDetail.add(model)
                    }
                    
                    DispatchQueue.main.async {
                        self.slapView.tblSlap.reloadData()
                    }
                    
                }
                
            } else {
                print(error)
                // SPAppData.sharedInstance.showAlert(title: "", message: "Something went wrong", viewController: self)
            }
            
        }
    }
    
    
    //MARK:- UITableView DataSource and Delegate
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrSlapDetail.count > 0 {
            return self.arrSlapDetail.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "SlapTableCell"
        let cell: SlapTableCell = (tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? SlapTableCell)!
        cell.selectionStyle = .none
        cell.delegate = self
        if self.arrSlapDetail.count > 0 {
            let model = self.arrSlapDetail[indexPath.row] as? SlapDetailModel
            cell.lblTitle.text = model?.desc
        }
        
        if (indexPath.row%3) == 0 {
            cell.imgTblcell.image = UIImage(named: "img_cell1")
        } else if ((indexPath.row - 1)%3) == 0 {
            cell.imgTblcell.image = UIImage(named: "img_cell2")
        } else {
            cell.imgTblcell.image = UIImage(named: "img_cell3")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrSlapDetail.count > 0 {
            return 60
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.arrSlapDetail.count > 0 {
            let model = self.arrSlapDetail[indexPath.row] as? SlapDetailModel
            
            //For Copy
            let paste: UIPasteboard = UIPasteboard.general
            paste.string = model?.desc
            
            (textDocumentProxy as UIKeyInput).insertText("MY SLAPBOT JUST SAID: \(paste.string!) \n\n\nwww.slapbot.com")
            
            //For Paste
            /*if let myString = paste.string {
             myTextField.insertText(myString)
             }*/
            
            print("Copy Table Cell Text:- \(String(describing: paste.string))")
        }
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        if(orientation == .right)
        {
        
            let favAction = SwipeAction(style: .default, title: "Favorites") { action, indexPath in
            // handle action by updating model with deletion
            tableView.setEditing(false, animated: true)
            tableView.reloadData()
            self.fav.append((self.arrSlapDetail[indexPath.row] as! SlapDetailModel).id)
            UserDefaults.standard.set(self.fav, forKey: "Fav")
        }
        
        // customize the action appearance
        favAction.backgroundColor = UIColor(red: 231.0/255.0, green: 75.0/255.0, blue: 207.0/255.0, alpha: 1.0)
     //   favAction.image = #imageLiteral(resourceName: "Mah")
        
        return [favAction]
        }
        else if(orientation == .left)
        {
            let deleteAction = SwipeAction(style: .default, title: "Mah...") { action, indexPath in
                // handle action by updating model with deletion
                self.arrSlapDetail.removeObject(at: indexPath.row)
                tableView.setEditing(false, animated: true)
                tableView.reloadData()
            }
           deleteAction.backgroundColor = UIColor(red: 52.0/255.0, green: 84.0/255.0, blue: 115.0/255.0, alpha: 1.0)
            // customize the action appearance
           // deleteAction.image = #imageLiteral(resourceName: "fav")
            
            return [deleteAction]
        }
        else
        {
            return nil
        }
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        tableView.setEditing(false, animated: true)
    }
  
    
    
//    @available(iOSApplicationExtension 11.0, *)
//    func tableView(_ tableView: UITableView,
//                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
//    {
//        let mehAction = UIContextualAction(style: .normal, title:  "Mah...", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
//            print("Meh Action")
//            self.arrSlapDetail.removeObject(at: indexPath.row)
//            tableView.reloadData()
//            success(true)
//        })
//        //UIImage(named: "ic_new")
//
//        mehAction.image = UIGraphicsImageRenderer(size:CGSize(width: 30, height: 30)).image { _ in
//            UIImage(named:"ic_new")?.draw(in: CGRect(x: 0, y: 0, width: 30, height: 30))
//        }
//
//
//        mehAction.backgroundColor = UIColor(red: 52.0/255.0, green: 84.0/255.0, blue: 115.0/255.0, alpha: 1.0)
//
//        return UISwipeActionsConfiguration(actions: [mehAction])
//
//    }
//
//    @available(iOSApplicationExtension 11.0, *)
//    func tableView(_ tableView: UITableView,
//                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
//    {
//        let favoriteAction = UIContextualAction(style: .normal, title:  "Favorite", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
//            print("Favorite action ...")
//            self.fav.append((self.arrSlapDetail[indexPath.row] as! SlapDetailModel).id)
//            UserDefaults.standard.set(self.fav, forKey: "Fav")
//            success(true)
//        })
//        //     UIImage(named: "ic_fav")
//        favoriteAction.image = UIGraphicsImageRenderer(size:CGSize(width: 30, height: 30)).image { _ in
//            UIImage(named:"ic_new")?.draw(in: CGRect(x: 0, y: 0, width: 30, height: 30))
//        }
//
//        favoriteAction.backgroundColor = UIColor(red: 231.0/255.0, green: 75.0/255.0, blue: 207.0/255.0, alpha: 1.0)
//        return UISwipeActionsConfiguration(actions: [favoriteAction])
//    }
    
    
    
    
    
    //MARK:- Gradiant
    
//    func setGradientBackground(view : UIContextualAction) {
//        let colorTop =  UIColor(red: 255.0/255.0, green: 149.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
//        let colorBottom = UIColor(red: 255.0/255.0, green: 94.0/255.0, blue: 58.0/255.0, alpha: 1.0).cgColor
//
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.colors = [ colorTop, colorBottom]
//        gradientLayer.locations = [ 0.0, 1.0]
//        gradientLayer.frame = self.view.bounds
//
//
//    }
    
    
    //MARK:- UICollectionView DataSource and Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.arrProductList.count > 0 {
            return self.arrProductList.count + 1
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let identifier = "SlapCollectionCell"
        let cell: SlapCollectionCell = (collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? SlapCollectionCell)!
        
        if self.arrProductList.count > 0{
            if(indexPath.row == self.arrProductList.count)
            {
                cell.lblSlapTitle.text = "Favorites"
            }
            else
            {
                let model: SlapProductModel = self.arrProductList[indexPath.item] as! SlapProductModel
                cell.lblSlapTitle.text = model.productName
            }
        }
        
        if indexPath.row == 0 || indexPath.row == 4 || indexPath.row == 6 {
            cell.lblSlapTitle.backgroundColor = UIColor(red: 34.0/255.0, green: 146.0/255.0, blue: 246.0/255.0, alpha: 1.0)
        } else if indexPath.row == 1 {
            cell.lblSlapTitle.backgroundColor = UIColor(red: 249.0/255.0, green: 81.0/255.0, blue: 133.0/255.0, alpha: 1.0)
        } else if indexPath.row == 2 {
            cell.lblSlapTitle.backgroundColor = UIColor(red: 156.0/255.0, green: 101.0/255.0, blue: 211.0/255.0, alpha: 1.0)
        } else if indexPath.row == 3 {
            cell.lblSlapTitle.backgroundColor = UIColor(red: 34.0/255.0, green: 161.0/255.0, blue: 69.0/255.0, alpha: 1.0)
        } else {
            cell.lblSlapTitle.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 1.0)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.arrProductList.count > 0 {
            if(indexPath.row == self.arrProductList.count)
            {
                
                let title: String? = "Favorites"
                let attributes = [NSAttributedStringKey.font: UIFont(name: "PingFangTC-Regular", size: 15.0)]
                let rect: CGRect? = title?.boundingRect(with: CGSize(width: 9999, height: 40), options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
                let size = CGSize(width: (rect?.size.width)! + 45, height: 30)
                return size
            }
            else
            {
                let model = self.arrProductList[indexPath.item] as? SlapProductModel
                let title: String? = model?.productName
                let attributes = [NSAttributedStringKey.font: UIFont(name: "PingFangTC-Regular", size: 15.0)]
                let rect: CGRect? = title?.boundingRect(with: CGSize(width: 9999, height: 40), options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
                let size = CGSize(width: (rect?.size.width)! + 45, height: 30)
                return size
            }
        } else {
            return CGSize(width: 0, height: 0)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(indexPath.row == self.arrProductList.count)
        {
            self.slapView.viewPurchase.isHidden = true
            getFav()
        }
        else
        {
            if(indexPath.row == 0)
            {
                self.slapView.viewPurchase.isHidden = true
                getSlapDetail(product: "\((dic[(arrProductList[indexPath.row] as! SlapProductModel).productName])!)", isSearch: false, searchString: "")
            }
            else
            {
       
                self.slapView.productImage.sd_setImage(with: URL(string: "\((self.arrProductList[indexPath.row] as! SlapProductModel).productBanner)"), completed: nil)
                self.slapView.btnPurchase.setTitle("Unlock \((self.arrProductList[indexPath.row] as! SlapProductModel).productCount) More \((self.arrProductList[indexPath.row] as! SlapProductModel).productshortname)", for: .normal)
             
                
                let defaults = UserDefaults(suiteName: "group.com.slap.Slapbot.slapBotKeyboard")
                defaults?.synchronize()
                if let value = defaults?.value(forKey: "\((dic[(arrProductList[indexPath.row] as! SlapProductModel).productName])!)")
                {
                    print(value)
                    self.slapView.viewPurchase.isHidden = true
                    getSlapDetail(product: "\((dic[(arrProductList[indexPath.row] as! SlapProductModel).productName])!)", isSearch: false, searchString: "")
                }
                else
                {
                    self.slapView.viewPurchase.isHidden = false
                }
                selectedProduct = indexPath.row
            }
        }
    }

    
   
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
