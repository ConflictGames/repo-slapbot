//
//  SlapTableCell.swift
//  Slapbot
//
//  Created by Nikul Dholiya on 23/01/18.
//  Copyright © 2018 Khatri Dhruv. All rights reserved.
//

import UIKit
import SwipeCellKit
class SlapTableCell: SwipeTableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTblcell: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
