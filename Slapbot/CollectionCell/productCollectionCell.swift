//
//  productCollectionCell.swift
//  Slapbot
//
//  Created by dhruv Khatri on 01/02/18.
//  Copyright © 2018 Khatri Dhruv. All rights reserved.
//

import UIKit

class productCollectionCell: UICollectionViewCell {

    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnPurchase: UIButton!
    @IBOutlet weak var btnTick: UIButton!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblRatings: UILabel!
    @IBOutlet weak var lblCoun: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
