//
//  SlapCollectionCell.swift
//  Slapbot
//
//  Created by Nikul Dholiya on 23/01/18.
//  Copyright © 2018 Khatri Dhruv. All rights reserved.
//

import UIKit

class SlapCollectionCell: UICollectionViewCell {

    @IBOutlet weak var lblSlapTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.lblSlapTitle.layer.cornerRadius = 15.0
        self.lblSlapTitle.layer.masksToBounds = true
        
    }

}
