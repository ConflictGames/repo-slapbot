//
//  IntroCell.swift
//  Slapbot
//
//  Created by dhruv Khatri on 20/02/18.
//  Copyright © 2018 Khatri Dhruv. All rights reserved.
//

import UIKit

class IntroCell: UICollectionViewCell {

    @IBOutlet weak var lblStep: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var imgIntro: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
