//
//  SlapProductModel.swift
//  Slapbot
//
//  Created by Nikul Dholiya on 01/02/18.
//  Copyright © 2018 Khatri Dhruv. All rights reserved.
//

import UIKit

class SlapProductModel: NSObject {
    
    var prodId = Int()
    var productName = String()
    
    override init() {
        
    }
    
    init(attributeDict:NSDictionary) {
        
        if let ids = attributeDict.value(forKey: "prodId") as? Int
        {
            prodId = ids
        }
        
        if let pName = attributeDict.value(forKey: "productName") as? String
        {
            productName = pName
        }
        
    }
    
}
