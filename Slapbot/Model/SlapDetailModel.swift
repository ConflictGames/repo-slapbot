//
//  SlapDetailModel.swift
//  Slapbot
//
//  Created by Nikul Dholiya on 01/02/18.
//  Copyright © 2018 Khatri Dhruv. All rights reserved.
//

import UIKit

class SlapDetailModel: NSObject {
    
    var id = Int()
    var rating = String()
    var imageUrl = String()
    var productImageSML = String()
    var actionword = String()
    var desc = String()
    var productImageMED = String()
    var productImageLG = String()
    var contentImageSML = String()
    var contentImageMED = String()
    var contentImageLG = String()
    var contentImageCard = String()
    var isAd = Bool()
    var productID = Int()
    var buyLink = String()
    var packID = String()
    var packageName = String()
    var audiofile = String()
    
    override init()
    {
        
    }
    
    init(attributeDict:NSDictionary) {
        
        if let ids = attributeDict.value(forKey: "id") as? Int
        {
            id = ids
        }
        
        if let rate = attributeDict.value(forKey: "rating") as? String
        {
            rating = rate
        }
        
        if let imgURL = attributeDict.value(forKey: "imageUrl") as? String
        {
            imageUrl = imgURL
        }
        
        if let prodImgSML = attributeDict.value(forKey: "productImageSML") as? String
        {
            productImageSML = prodImgSML
        }
        
        if let actionWord = attributeDict.value(forKey: "actionword") as? String
        {
            actionword = actionWord
        }
        
        if let des = attributeDict.value(forKey: "description") as? String
        {
            desc = des
        }
        
        if let aFile = attributeDict.value(forKey: "audiofile") as? String
        {
            audiofile = aFile
        }
        
        if let prodImgMED = attributeDict.value(forKey: "productImageMED") as? String
        {
            productImageMED = prodImgMED
        }
        
        if let prodImgLG = attributeDict.value(forKey: "productImageLG") as? String
        {
            productImageLG = prodImgLG
        }
        
        if let contImgSML = attributeDict.value(forKey: "contentImageSML") as? String
        {
            contentImageSML = contImgSML
        }
        
        if let contImgMED = attributeDict.value(forKey: "contentImageMED") as? String
        {
            contentImageMED = contImgMED
        }
        
        if let contImgLG = attributeDict.value(forKey: "contentImageLG") as? String
        {
            contentImageLG = contImgLG
        }
        
        if let contImgCard = attributeDict.value(forKey: "contentImageCard") as? String
        {
            contentImageCard = contImgCard
        }
        
        if let ad = attributeDict.value(forKey: "isAd") as? Bool
        {
            isAd = ad
        }
        
        if let prodID = attributeDict.value(forKey: "productID") as? Int
        {
            productID = prodID
        }
        
        if let bLink = attributeDict.value(forKey: "buyLink") as? String
        {
            buyLink = bLink
        }
        
        if let pID = attributeDict.value(forKey: "packID") as? String
        {
            packID = pID
        }
        
        if let pName = attributeDict.value(forKey: "packageName") as? String
        {
            packageName = pName
        }
        
    }

}
