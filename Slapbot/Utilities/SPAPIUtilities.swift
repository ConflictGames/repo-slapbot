//
//  DDAPIUtilities.swift
//  doggydog
//
//  Created by My PC on 6/1/17.
//  Copyright © 2017 My PC. All rights reserved.
//


import UIKit
import Alamofire
//import SVProgressHUD

class SPAPIUtilities: NSObject {
    
    class var sharedInstance : SPAPIUtilities {
        struct Static {
            static let instance : SPAPIUtilities = SPAPIUtilities()
        }
        return Static.instance
    }
    
    var manager: SessionManager {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 8
        manager.session.configuration.timeoutIntervalForResource = 8
        return manager
    }
    
    //MARK:- GET
    func GetAPICallWith(url:String, completionHandler:@escaping (AnyObject?, NSError?)->()) ->()
    {
        /*if(self.checkNetworkConnectivity() == "NoAccess"){
            self.callNetworkAlert()
            SVProgressHUD.dismiss()
            return;
        }*/
        
        print("------------------------------------------------------------------")
        print("GET URL: ",url)
        print("------------------------------------------------------------------")
        
        /*var header = [String:String]()
        if let key = UserDefaults.standard.value(forKey: "firebaseIDToken") as? String
        {
            header = ["Authorization":"key="+key,"Content-Type":"application/json","Host":"cardex-node-prod01.appspot.com"]
            print("header===>",header)
        }
        else
        {
            header = ["Content-Type": "application/json"]
        }*/
        
        manager.request(url, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .responseString(completionHandler: { (responsestr) in
                print(responsestr)
            })
            .responseJSON { response in

                if let JSON = response.result.value {
                    completionHandler(JSON as AnyObject?, nil)
                }
                else {
                    print(response.result.error ?? "")
                    completionHandler(nil, response.result.error! as NSError)
                }
        }
    }
    
    func DeleteAPICallWith(url:String, completionHandler:@escaping (AnyObject?, NSError?)->()) ->()
    {
//        if(self.checkNetworkConnectivity() == "NoAccess"){
//            self.callNetworkAlert()
//           // SVProgressHUD.dismiss()
//            return;
//        }
        
        print("------------------------------------------------------------------")
        print("DELETE URL: ",url)
        print("------------------------------------------------------------------")
        
        var header = [String:String]()
        if let key = UserDefaults.standard.value(forKey: "firebaseIDToken") as? String
        {
            header = ["Authorization":"key="+key,"Content-Type":"application/json","Host":"cardex-node-prod01.appspot.com"]
            print("header===>",header)
        }
        else
        {
            header = ["Content-Type": "application/json"]
        }
        
        manager.request(url, method: HTTPMethod.delete, parameters: nil, encoding: JSONEncoding.default, headers: header)
                        .responseString(completionHandler: { (responsestr) in
                            print(responsestr)
                        })
            .responseJSON { response in
                
                
                
                if let JSON = response.result.value {
                    completionHandler(JSON as AnyObject?, nil)
                }
                else {
                    print(response.result.error ?? "")
                    completionHandler(nil, response.result.error! as NSError)
                }
        }
    }
    
    //MARK:- POST
    func postResponseAPI(url:String, param:AnyObject, completionHandler:@escaping (AnyObject?, NSError?)->()) ->()
    {
//        if(self.checkNetworkConnectivity() == "NoAccess"){
//            self.callNetworkAlert()
//         //   SVProgressHUD.dismiss()
//            return;
//        }
        
        print("------------------------------------------------------------------")
        print("POST URL: ",url)
        print("Request Param: ",param)
        print("------------------------------------------------------------------")
        
        var header = [String:String]()
        if let key = UserDefaults.standard.value(forKey: "firebaseIDToken") as? String
        {
            header = ["Authorization":"key="+key,"Content-Type":"application/json","Host":"cardex-node-prod01.appspot.com"]
            print("header===>",header)
            print("param ===>",param)
        }
        else
        {
            header = ["Content-Type": "application/json"]
        }
        
        manager.request(url, method: HTTPMethod.post, parameters: param as? Parameters, encoding: JSONEncoding.default, headers: header)
            .responseString(completionHandler: { (responsestr) in
                print("response ===> ",responsestr)
            })
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    completionHandler(JSON as AnyObject?, nil)
                }
                else {
                    print(response.result.error ?? "")
                    completionHandler(nil, response.result.error! as NSError)
                }
        }
    }
    
    //MARK:- POST
    func POSTPushTokenToServer(url:String, param:AnyObject, completionHandler:@escaping (AnyObject?, NSError?)->()) ->()
    {
//        if(self.checkNetworkConnectivity() == "NoAccess"){
//            self.callNetworkAlert()
//        //    SVProgressHUD.dismiss()
//            return;
//        }
     
        print("------------------------------------------------------------------")
        print("POST URL: ",url)
        print("Request Param: ",param)
        print("------------------------------------------------------------------")
        
        var header = [String:String]()
        if let key = UserDefaults.standard.value(forKey: "firebaseIDToken") as? String {
            header = ["Authorization":"key="+key,"Content-Type":"application/json","Host":"cardex-node-prod01.appspot.com"]
            print("header===>",header)
        } else {
            header = ["Content-Type": "application/json"]
        }
        
        manager.request(url, method: .post, parameters: param as? Parameters, encoding: JSONEncoding.default, headers: header)
            .responseString(completionHandler: { (responsestr) in
                print("reponse===>",responsestr)
            })
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    completionHandler(JSON as AnyObject?, nil)
                }
                else {
                    print(response.result.error ?? "")
                    completionHandler(nil, response.result.error! as NSError)
                }
        }
    }
    
    //MARK:- PUT API
    func PutResponseAPI(url:String, param:AnyObject, completionHandler:@escaping (AnyObject?, NSError?)->()) ->()
    {
        
//        if(self.checkNetworkConnectivity() == "NoAccess"){
//            self.callNetworkAlert()
//         //   SVProgressHUD.dismiss()
//            return;
//        }
        
        print("URL====>",url)
        
        var header = [String:String]()
        if let key = UserDefaults.standard.value(forKey: "firebaseIDToken") as? String
        {
            header = ["Authorization":"key="+key,"Content-Type":"application/json","Host":"cardex-node-prod01.appspot.com"]
            print("header===>",header)
        }
        else
        {
            header = ["Content-Type": "application/json"]
        }
        
        manager.request(url, method: .put, parameters: param as? Parameters, encoding: JSONEncoding.default, headers: header)
            .responseString(completionHandler: { (responsestr) in
                print("reponse===>",responsestr)
            })
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    completionHandler(JSON as AnyObject?, nil)
                }
                else {
                    print(response.result.error ?? "")
                    completionHandler(nil, response.result.error! as NSError)
                }
        }
    }

    //MARK:- Image Uploading
    func uploadImage(image:UIImage, url:String, param:NSDictionary, completionHandler:@escaping (AnyObject?, NSError?)->()) ->() {
        
        manager.upload(multipartFormData: { multipartFormData in
            let data = UIImageJPEGRepresentation(image, 0.6)
            multipartFormData.append(data!, withName: "url", fileName: "image", mimeType: "Image/jpeg")
            for (key, value) in param {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
            
        },
                         to: url,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    debugPrint("Response",response)
                                    if let JSON = response.result.value {
                                        completionHandler(JSON as AnyObject?, nil)
                                    }
                                }
                            case .failure(let encodingError):
                                print(encodingError)
                                completionHandler(nil,encodingError as NSError?)
                            }
        })
    }
    
    
    //MARK:- DELETE
    func deleteAPICall(url:String, completionHandler:@escaping (AnyObject?, NSError?)->()) ->()
    {
//        if(self.checkNetworkConnectivity() == "NoAccess"){
//            self.callNetworkAlert()
//         //   SVProgressHUD.dismiss()
//            return;
//        }
        
        let header = [String:String]()
        
        manager.request(url, method: HTTPMethod.delete, parameters: nil, encoding: JSONEncoding.default, headers: header as [String:String])
            .responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)   // result of response serialization
                if let JSON = response.result.value
                {
                    if response.response?.statusCode == 201
                    {
                        completionHandler(JSON as AnyObject?, nil)
                    }
                    else if response.response?.statusCode == 200
                    {
                        completionHandler(JSON as AnyObject?, nil)
                    }
                    else if response.response?.statusCode == 202
                    {
                        completionHandler(JSON as AnyObject?, nil)
                    }
                    else if response.response?.statusCode == 204
                    {
                        completionHandler(JSON as AnyObject?, nil)
                    }
                    else if  response.response?.statusCode == 400
                    {
                 //       SVProgressHUD.dismiss()
                        completionHandler(nil,response.result.error as NSError?)
                        
                        if let dict = JSON as? NSDictionary {
                            let dataDict = dict.value(forKey: "meta") as! NSDictionary
                            let error = dataDict.value(forKeyPath: "error") as! NSDictionary
                            self.ShowAlert(title: "Whoops", messsage: error.value(forKey: "message") as! String)
                        }
                    }
                    else if  response.response?.statusCode == 404
                    {
                   //     SVProgressHUD.dismiss()
                        completionHandler(nil,response.result.error as NSError?)
                        
                        if let dict = JSON as? NSDictionary {
                            let dataDict = dict.value(forKey: "meta") as! NSDictionary
                            let error = dataDict.value(forKeyPath: "error") as! NSDictionary
                            self.ShowAlert(title: "Whoops", messsage: error.value(forKey: "message") as! String)
                        }
                    }
                    else
                    {
                        completionHandler(nil,response.result.error as NSError?)
                    }
                }
                else
                {
                    completionHandler(nil,response.result.error as NSError?)
                }
        }
    }
    
//    //MARK: Network Check
//    func checkNetworkConnectivity()->String
//    {
//        let network: Reachability = Reachability();
//
//        var networkValue:String = "" as String
//
//
//
//        if(network.currentReachabilityStatus().rawValue == 0)
//        {
//            networkValue = "NoAccess";
//        }
//        else if(network.currentReachabilityStatus().rawValue == 1)
//        {
//            networkValue = "e";
//        }
//        else if(network.currentReachabilityStatus().rawValue == 2)
//        {
//            networkValue = "wifi";
//        }
//        else  if(network.currentReachabilityStatus().rawValue == 3)
//        {
//            networkValue = "3g";
//        }
//
//        return networkValue;
//    }
    
    func callNetworkAlert()
    {
        let alert = UIAlertView()
        alert.title = "No Network Found!"
        alert.message = "Please check your internet connection."
        alert.addButton(withTitle: "OK")
        alert.show()
    }
    
    func ShowAlert(title: String, messsage: String)
    {
        let alert = UIAlertView()
        alert.title = title
        alert.message = messsage
        alert.addButton(withTitle: "OK")
        alert.show()
    }
    
    func callBadNetworkAlert()
    {
        let alert = UIAlertView()
        alert.title = "Bad Internet Connection!"
        alert.message = "Please check your internet connection."
        alert.addButton(withTitle: "OK")
        alert.show()
    }
    
}

class Request : NSObject {
    func send(url: String, f: (String)-> ()) {
        let request = NSURLRequest(url: URL(string: url)!)
        var response: URLResponse?
        let data = try? NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
        let reply = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        f(reply! as String)
    }
    
    func sendFile(urlPath:String, type:String, fileName:String,  data:Data, completionHandler: @escaping (NSDictionary?) -> Void){
        
//        if(SPAPIUtilities.sharedInstance.checkNetworkConnectivity() == "NoAccess"){
//            SPAPIUtilities.sharedInstance.callNetworkAlert()
//       //     SVProgressHUD.dismiss()
//            return;
//        }
        
        let url: URL = URL(string: urlPath.appending("&type=\(type)"))!
        let request1: NSMutableURLRequest = NSMutableURLRequest(url: url )
        
        request1.httpMethod = "POST"
        
        let boundary = generateBoundaryString()
        let fullData = photoDataToFormData(data: data,boundary:boundary,fileName:fileName, type:type)
        
        request1.setValue("multipart/form-data; boundary=" + boundary, forHTTPHeaderField: "Content-Type")
        
        // REQUIRED!
        request1.setValue(String(fullData.length), forHTTPHeaderField: "Content-Length")
        request1.httpBody = fullData as Data
        request1.httpShouldHandleCookies = false
        var response: URLResponse?
        
        do
        {
            let jsonData = try NSURLConnection.sendSynchronousRequest(request1 as URLRequest, returning: &response)
            
            let jsonDict = try? JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            completionHandler(jsonDict as NSDictionary?)
            
        }
        catch
        {
            
        }
    }
    
    func photoDataToFormData(data:Data,boundary:String,fileName:String, type:String) -> NSData {
        let fullData = NSMutableData()
        
        let lineOne = "--" + boundary + "\r\n"
        fullData.append(lineOne.data(
            using: String.Encoding.utf8,
            allowLossyConversion: false)!)
        
        // 2
        
        let lineTwo = "Content-Disposition: form-data; name=\"url\"; filename=\"" + fileName + "\"\r\n"
        NSLog(lineTwo)
        
        fullData.append(lineTwo.data(
            using: String.Encoding.utf8,
            allowLossyConversion: false)!)
        
        // 3
        let lineThree = "Content-Type: image/jpg\r\n\r\n"
        fullData.append(lineThree.data(
            using: String.Encoding.utf8,
            allowLossyConversion: false)!)
        
        // 4
        fullData.append(data as Data)
        
        // 5
        let lineFive = "\r\n"
        fullData.append(lineFive.data(
            using: String.Encoding.utf8,
            allowLossyConversion: false)!)
        
        // 6 - The end. Notice -- at the start and at the end
        let lineSix = "--" + boundary + "--\r\n"
        fullData.append(lineSix.data(
            using: String.Encoding.utf8,
            allowLossyConversion: false)!)
        
        return fullData
    }
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}

