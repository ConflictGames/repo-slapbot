//
//  SPAppData.swift
//
//
//  Created by pc1 on 25/09/17.
//  Copyright © 2017. All rights reserved.
//

import UIKit
//import SVProgressHUD

class SPAppData: NSObject {
    
    class var sharedInstance : SPAppData {
        
        struct Static {
            static let instance : SPAppData = SPAppData()
        }
        return Static.instance
    }
    
    func getScreenHeight() -> CGFloat {
        return UIScreen.main.bounds.height
    }
    
    func getScreenWidth() -> CGFloat {
        return UIScreen.main.bounds.width
    }
    
    func setCornerRadius( view: AnyObject, radius: CGFloat) {
        (view as? UIView)!.layer.cornerRadius = radius
        (view as? UIView)!.layer.masksToBounds = true
    }
    
    
    
    func isValidatePresence(string: String) -> Bool {
        let trimmed: String = string.trimmingCharacters(in: .whitespacesAndNewlines)
        return !trimmed.isEmpty
    }
    
    func isValidEmail(testStr: String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let isValid = emailTest.evaluate(with: testStr)
        return isValid
    }
    
    func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let isValid = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
        return isValid
    }
    
    func isValidURL(string: String?) -> Bool {
        guard let urlString = string else {return false}
        guard let url = NSURL(string: urlString) else {return false}
    //    if !UIApplication.shared.canOpenURL(url as URL) {return false}
//
        //
        //let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let regEx = "((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        //let regEx = "((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
        return predicate.evaluate(with: string)
    }
    
  //  func topMostController() -> UIViewController
//    {
//        var topController : UIViewController = (UIApplication.shared.keyWindow?.rootViewController)!
//        while ((topController.presentedViewController) != nil)
//        {
//            topController = topController.presentedViewController!
//        }
//        return topController
   // }
    
    func showAlert(title: String, message: String, viewController : UIViewController)->Void {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let action1 = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel) { (dd) -> Void in
        }
        alert.addAction(action1)
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func showLoader() {
//        SVProgressHUD.show()
//        SVProgressHUD.setDefaultMaskType(.gradient)
    }
    
    func dismissLoader() {
       // SVProgressHUD.dismiss()
    }
    
    
}
