//
//  SBKeyboardSettingVC.swift
//  Slapbot
//
//  Created by Nikul Dholiya on 28/01/18.
//  Copyright © 2018 Khatri Dhruv. All rights reserved.
//

import UIKit

class SBKeyboardSettingVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var colSteps: UICollectionView!
    @IBOutlet weak var btnEnableSettings: UIButton!
    @IBOutlet weak var btnRemeberMe: UIButton!
    @IBOutlet weak var startScreen: UIView!
    
    var img = [#imageLiteral(resourceName: "1"),#imageLiteral(resourceName: "2"),#imageLiteral(resourceName: "3"),#imageLiteral(resourceName: "4")]
    var text = ["Go to settings and select keyboard","go To keyboards","select slap and click on it","go to slap and allow full access to enjoy slaps"]
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set("yes", forKey: "settings")
        colSteps.delegate = self
        colSteps.dataSource = self
        colSteps.register(UINib(nibName: "IntroCell", bundle: nil), forCellWithReuseIdentifier: "IntroCell")
        self.navigationController?.navigationBar.isHidden = true
        
        if let show = UserDefaults.standard.value(forKey: "show") as? Bool
        {
            if(show)
            {
                //PrintShow
                startScreen.isHidden = false
                let loginVC = storyboard?.instantiateViewController(withIdentifier: "SBLoginVC") as! SBLoginVC
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            else
            {
                startScreen.isHidden = true
                
            }
        }
        else
        {
            startScreen.isHidden = true
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSettingsClicked(_ sender: UIButton) {
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
    
    @IBAction func onTapBtnEnablesettings(_ sender: Any) {
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let slapVC = storyboard.instantiateViewController(withIdentifier: "SlapVC") as! SlapVC
        self.navigationController?.pushViewController(slapVC, animated: true)
        
    }
    
    @IBAction func onTapBtnRemeberMe(_ sender: Any) {
        self.btnRemeberMe.isSelected = !self.btnRemeberMe.isSelected
        
        if self.btnRemeberMe.isSelected {
            self.btnRemeberMe.setImage(UIImage(named: "ic_check") , for: UIControlState.normal)
        } else {
            self.btnRemeberMe.setImage(UIImage(named: "ic_uncheck"), for: UIControlState.normal)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    @IBAction func btnSetupComplete(_ sender: UIButton) {
        UserDefaults.standard.set(btnRemeberMe.isSelected, forKey: "show")
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "SBLoginVC") as! SBLoginVC
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCell", for: indexPath) as! IntroCell
        cell.imgIntro.image = img[indexPath.row]
        cell.lblDetail.text = "\(text[indexPath.row])"
        cell.lblStep.text = "Step \(indexPath.row + 1)"
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: colSteps.frame.width, height: colSteps.frame.height)
    }
}
