//
//  SBPurchaseVC.swift
//  Slapbot
//
//  Created by dhruv Khatri on 01/02/18.
//  Copyright © 2018 Khatri Dhruv. All rights reserved.
//

import UIKit
import StoreKit
import SDWebImage

class SBPurchaseVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var lblTotalSelectedNumber: UILabel!
    @IBOutlet weak var colProductList: UICollectionView!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    @IBOutlet weak var imgSelectAll: UIImageView!
    @IBOutlet weak var imgWhatsNew: UIImageView!
    @IBOutlet weak var imgOnSale: UIImageView!
    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var btnUnlock: UIButton!
    @IBOutlet weak var purchaseActivity: UIActivityIndicatorView!
    
    
    enum IAPHandlerAlertType{
        case disabled
        case restored
        case purchased
        
        func message() -> String{
            switch self {
            case .disabled: return "Purchases are disabled in your device!"
            case .restored: return "You've successfully restored your purchase!"
            case .purchased: return "You've successfully bought this purchase!"
            }
        }
    }
    
    
    var dic = ["Slaps" : "Slaps",
               "Hairline Jokes" : "Hairline insults",
               "Appreciations" : "I Appreciation you like",
               "I Love You More Than" : "I lOVE YOU MORE THAN",
               "Tongue Twisters" : "Tongue Twisters"]
    
    var dic2 = ["Slaps" : "slap",
               "Hairline Jokes" : "hairline",
               "Appreciations" : "appreciate",
               "I Love You More Than" : "love",
               "Tongue Twisters" : "Twister"]
    
    var dic3 = ["Slaps" : "Slaps",
               "Hairline insults" : "Hairline Jokes",
               "I Appreciation you like" : "Appreciations",
               "I lOVE YOU MORE THAN" : "I Love You More Than",
               "Tongue Twisters" : "Tongue Twisters"]
    
    
    var arrProductList = NSMutableArray()
    
    static let shared = IAPHandler()
    
    let Hairline_insults = "Hairline_insults"
    let I_Appreciation_you_like = "I_Appreciation_you_like"
    let I_lOVE_YOU_MORE_THAN = "I_lOVE_YOU_MORE_THAN"
    let Tongue_Twisters = "Tongue_Twisters"
    
    
    var productID = "12345678"
    var productsRequest = SKProductsRequest()
    var iapProducts = [SKProduct]()
    var purchasedText = [String]()
    
    var productName = [String : String]()
    
    var purchaseStatusBlock: ((IAPHandlerAlertType) -> Void)?
    var selectedProduct : String = ""
    var selectedProductIndex : Int = -1
    // MARK: - MAKE PURCHASE OF A PRODUCT
    func canMakePurchases() -> Bool {  return SKPaymentQueue.canMakePayments()  }
    
    func purchaseMyProduct(index: Int){
        if iapProducts.count == 0 { return }
        
        if self.canMakePurchases() {
            let product = iapProducts[index]
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
            
            print("PRODUCT TO PURCHASE: \(product.productIdentifier)")
            productID = product.productIdentifier
        } else {
            purchaseStatusBlock?(.disabled)
        }
    }
    
    // MARK: - RESTORE PURCHASE
    func restorePurchase(){
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    
    // MARK: - FETCH AVAILABLE IAP PRODUCTS
    func fetchAvailableProducts(){
        
        // Put here your IAP Products ID's
        let productIdentifiers = NSSet(objects: Tongue_Twisters,I_lOVE_YOU_MORE_THAN,Hairline_insults,I_Appreciation_you_like
        )
        
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
        productsRequest.delegate = self
        productsRequest.start()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        purchaseActivity.startAnimating()
        purchaseActivity.isHidden = true
        
        //UserDefaults.standard.set("true", forKey: "Love")
        btnUnlock.setTitle("Already Unlocked", for: .normal)
       // print(UserDefaults.standard.value(forKey: "Google"))
        fetchAvailableProducts()
        restorePurchase()
        getProductList()
//        purchaseStatusBlock = {[weak self] (type) in
//            guard let strongSelf = self else {
//                return
//            }
//            if type == .purchased
//            {
//                let alertView = UIAlertController(title: "", message: type.message(), preferredStyle: .alert)
//                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
//
//                })
//                alertView.addAction(action)
//                strongSelf.present(alertView, animated: true, completion: nil)
//            }
//        }
        
        colProductList.dataSource = self
        colProductList.delegate = self
        colProductList.register(UINib(nibName: "productCollectionCell", bundle: nil), forCellWithReuseIdentifier: "productCollectionCell")
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBuyClicked(_ sender: UIButton) {
        
        purchaseMyProduct(index: 0)
    }
    
    @IBAction func btnOnSaleClicked(_ sender: UIButton) {
    }
    
    
    @IBAction func btnWhatNewClicked(_ sender: UIButton) {
    }
    
    @IBAction func btnSelectAllClicked(_ sender: UIButton) {
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return iapProducts.count
        return arrProductList.count
    }
    
    @objc func btnPurchase(sender : UIButton)
    {
//        var index = 0
//        for product in iapProducts
//        {
//            if(product.localizedDescription == dic[(arrProductList[sender.tag] as! SlapProductModel).productName])
//            {
//                purchaseMyProduct(index: index)
//            }
//            index = index + 1
//        }
        purchaseActivity.isHidden = false
        selectedProduct = dic2[dic3[iapProducts[sender.tag].localizedTitle]!]!
        purchaseMyProduct(index: sender.tag)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCollectionCell", for: indexPath) as! productCollectionCell
       // print(iapProducts[indexPath.row].localizedTitle)
        let currentProduct = arrProductList[indexPath.row] as! SlapProductModel
        cell.lblProductName.text = "\(currentProduct.productName)"
        cell.imgProduct.sd_setImage(with: URL(string: "\(currentProduct.productImage[currentProduct.productName]!)"), completed: nil)
        cell.lblRatings.text = "\(currentProduct.productRatings)"
        cell.lblCoun.text = "\(currentProduct.productCount)"
        var purchaseIndex = 0
        for index in iapProducts
        {
            if(dic[currentProduct.productName] == index.localizedTitle)
            {
                cell.lblPrice.text = "\(index.price) \(index.priceLocale.currencySymbol!)"
                 let defaults = UserDefaults(suiteName: "group.com.slap.Slapbot.slapBotKeyboard")
                defaults?.synchronize()
                if let value = defaults?.value(forKey: "\(dic2[currentProduct.productName]!)")
                {
                    print(value)
                    cell.btnPurchase.setTitle("OWNED", for: .normal)
                    //cell.lblPrice.text = "Already Purchased"
                    let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(index.price) \(index.priceLocale.currencySymbol!)")
                    attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                    cell.lblPrice.attributedText = attributeString
                }
                else
                {
                    cell.btnPurchase.setTitle("UNLOCK", for: .normal)
                }
                cell.btnPurchase.tag = purchaseIndex
                print(purchaseIndex)
                print(index.localizedTitle)
                cell.btnPurchase.addTarget(self, action: #selector(btnPurchase(sender:)), for: .touchUpInside)
            }
            purchaseIndex = purchaseIndex + 1
        }
        
        if(currentProduct.productName == "Slaps")
        {
            cell.lblPrice.text = "Free"
            //cell.btnPurchase.tag = purchaseIndex
            cell.btnPurchase.setTitle("OWNED", for: .normal)
            //cell.btnPurchase.addTarget(self, action: #selector(btnPurchase(sender:)), for: .touchUpInside)
        }
        
        //cell.lblPrice.text = "\(iapProducts[indexPath.row].price)"
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = colProductList.frame.height - 60
        let widthPer = height/300
        let width = 80*widthPer
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedProductIndex = indexPath.row - 1
        if(indexPath.row == 0)
        {
            btnUnlock.setTitle("Already Unlocked", for: .normal)
        }
        else
        {
            let currentProduct = arrProductList[indexPath.row] as! SlapProductModel
            for index in iapProducts
            {
                if(dic[currentProduct.productName] == index.localizedTitle)
                {
                    let defaults = UserDefaults(suiteName: "group.com.slap.Slapbot.slapBotKeyboard")
                    defaults?.synchronize()
                    if let value = defaults?.value(forKey: "\(dic2[currentProduct.productName]!)")
                    {
                        btnUnlock.setTitle("Already Unlocked", for: .normal)
                        
                    }
                    else
                    {
                        btnUnlock.setTitle("Unlock \((self.arrProductList[indexPath.row] as! SlapProductModel).productCount) More \((self.arrProductList[indexPath.row] as! SlapProductModel).productshortname) for \(iapProducts[selectedProductIndex].price) \(iapProducts[selectedProductIndex].priceLocale.currencySymbol!)", for: .normal)
                    }
              
                }
            }
            
        
        }
        self.imgBanner.sd_setImage(with: URL(string: "\((self.arrProductList[indexPath.row] as! SlapProductModel).productBanner)"), completed: nil)

    }
    
    func storeOffline()
    {
        restorePurchase()
    }
    
    
    
    @IBAction func btnUnlockClicked(_ sender: UIButton) {
        purchaseActivity.isHidden = false
        let currentProduct = arrProductList[selectedProductIndex + 1] as! SlapProductModel
        var purchaseIndex = 0
        for index in iapProducts
        {
            if(dic[currentProduct.productName] == index.localizedTitle)
            {
                
                selectedProduct = dic2[currentProduct.productName]!
                purchaseMyProduct(index: purchaseIndex)
                break
            }
            purchaseIndex = purchaseIndex + 1
        }
        
        
        
    }
    
    
    func getProductList() {
        
        let URL_Post = "http://conflictapi.com/v1/api/productlist?app_id=ac63d082&app_key=0fd23f82553331a53caa5b497708e8b3&allowedpackages=slapbot&activeonly=false&maxrecords=50"
        
        //    SPAppData.sharedInstance.showLoader()
        SPAPIUtilities.sharedInstance.GetAPICallWith(url: URL_Post) { (response, error) in
            //SPAppData.sharedInstance.dismissLoader()
            if error == nil {
                if let arrResponse = response as? NSArray{
                    for dic in arrResponse {
                        print(dic)
                        let model = SlapProductModel(attributeDict: dic as! NSDictionary)
                        self.arrProductList.add(model)
                    }
                    
                    DispatchQueue.main.async {
                        if(self.arrProductList.count > 0)
                        {
                            self.imgBanner.sd_setImage(with: URL(string: "\((self.arrProductList[0] as! SlapProductModel).productBanner)"), completed: nil)
                        }
                        //self.colProductList.reloadData()
                     //   self.slapView.collectionSlap.reloadData()
                    }
                    
                }
            } else {
                SPAppData.sharedInstance.showAlert(title: "", message: "Something went wrong", viewController: self)
            }
            
        }
        
    }
    
    @IBAction func btnLogoutClicked(_ sender: UIButton) {
    
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "password")

        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "SBLoginVC") as! SBLoginVC
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    
    
}
    

extension SBPurchaseVC: SKProductsRequestDelegate, SKPaymentTransactionObserver{
        // MARK: - REQUEST IAP PRODUCTS
        func productsRequest (_ request:SKProductsRequest, didReceive response:SKProductsResponse) {
            
            if (response.products.count > 0) {
                iapProducts = response.products
                for product in iapProducts{
                    let numberFormatter = NumberFormatter()
                    numberFormatter.formatterBehavior = .behavior10_4
                    numberFormatter.numberStyle = .currency
                    numberFormatter.locale = product.priceLocale
                    let price1Str = numberFormatter.string(from: product.price)
                    
                    print(product.localizedDescription + "\nfor just \(price1Str!)")
                }
                colProductList.reloadData()
            }
        }
        
        func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
            purchaseStatusBlock?(.restored)
        }
        
        // MARK:- IAP PAYMENT QUEUE
        func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
            for transaction:AnyObject in transactions {
                if let trans = transaction as? SKPaymentTransaction {
                    switch trans.transactionState {
                    case .purchased:
                        purchaseActivity.isHidden = true
                        print("purchased")
                        //UserDefaults.standard.set(true, forKey: "\(selectedProduct)")
                        let defaults = UserDefaults(suiteName: "group.com.slap.Slapbot.slapBotKeyboard")
                        defaults?.set(true, forKey: "\(selectedProduct)")
                        defaults?.synchronize()
                        SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                        purchaseStatusBlock?(.purchased)
                        colProductList.reloadData()
                        break
                        
                    case .failed:
                        purchaseActivity.isHidden = true
                        print("failed")
                        SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                        break
                    case .restored:
                        purchaseActivity.isHidden = true
                        print("restored")
                        let defaults = UserDefaults(suiteName: "group.com.slap.Slapbot.slapBotKeyboard")
                        defaults?.set(true, forKey: "\(selectedProduct)")
                        defaults?.synchronize()
                        colProductList.reloadData()
                       // UserDefaults.standard.set(true, forKey: "\(selectedProduct)")
                        SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                        break
                        
                    default: break
                    }}}
        }
    }


    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

