//
//  SlapVC.swift
//  Slapbot
//
//  Created by Nikul Dholiya on 23/01/18.
//  Copyright © 2018 Khatri Dhruv. All rights reserved.
//

import UIKit
//import SVProgressHUD

class SlapVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var slapView: SlapView2!
    
    var arrSlap = ["Slaps","Evil eyes","tongue twister","Your Moms","Insults","Favorites", "Recent"]
    var arrtblDesc = ["it never calculate the true means joy  until its robot palm collied  with face","slapped you hard  the terminator  just  gave me  his job","it will slap harder then new prisoners hold soap"]
    var arrSlapDetail = NSMutableArray()
    var arrProductList = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.slapView = Bundle.main.loadNibNamed("SlapView2", owner: self, options: nil)![0] as! SlapView2
        self.slapView.frame = CGRect(x: 0, y: 30, width: UIScreen.main.bounds.size.width, height: 250)
        self.view .addSubview(slapView)
        
        self.slapView.tblSlap.delegate = self
        self.slapView.tblSlap.dataSource = self
        self.slapView.tblSlap.showsVerticalScrollIndicator = false
        self.slapView.tblSlap.separatorStyle = .none
        
        self.slapView.collectionSlap.delegate = self
        self.slapView.collectionSlap.dataSource = self
        self.slapView.collectionSlap.showsHorizontalScrollIndicator = false
        
        self.slapView.collectionSlap.register(UINib(nibName: "SlapCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SlapCollectionCell")
        self.slapView.tblSlap.register(UINib(nibName: "SlapTableCell", bundle: nil), forCellReuseIdentifier: "SlapTableCell")

        self.setCornerRadius()
        self.setupKeyboard()
        self.getProductList()
        // Do any additional setup after loading the view.
    }
    
    func setCornerRadius() {
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblA, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblB, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblC, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblD, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblE, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblF, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblG, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblH, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblI, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblJ, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblK, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblL, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblM, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblN, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblO, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblP, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblQ, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblR, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblS, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblT, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblU, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblV, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblW, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblX, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblY, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.slapView.lblZ, radius: 3.0)
    }
    
    func setupKeyboard() {
        
        let tapA = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblA))
        self.slapView.lblA.isUserInteractionEnabled = true
        self.slapView.lblA.addGestureRecognizer(tapA)
        
        let tapB = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblB))
        self.slapView.lblB.isUserInteractionEnabled = true
        self.slapView.lblB.addGestureRecognizer(tapB)
        
        let tapC = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblC))
        self.slapView.lblC.isUserInteractionEnabled = true
        self.slapView.lblC.addGestureRecognizer(tapC)
        
        let tapD = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblD))
        self.slapView.lblD.isUserInteractionEnabled = true
        self.slapView.lblD.addGestureRecognizer(tapD)
        
        let tapE = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblE))
        self.slapView.lblE.isUserInteractionEnabled = true
        self.slapView.lblE.addGestureRecognizer(tapE)
        
        let tapF = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblF))
        self.slapView.lblF.isUserInteractionEnabled = true
        self.slapView.lblF.addGestureRecognizer(tapF)
        
        let tapG = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblG))
        self.slapView.lblG.isUserInteractionEnabled = true
        self.slapView.lblG.addGestureRecognizer(tapG)
        
        let tapH = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblH))
        self.slapView.lblH.isUserInteractionEnabled = true
        self.slapView.lblH.addGestureRecognizer(tapH)
        
        let tapI = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblI))
        self.slapView.lblI.isUserInteractionEnabled = true
        self.slapView.lblI.addGestureRecognizer(tapI)
        
        let tapJ = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblJ))
        self.slapView.lblJ.isUserInteractionEnabled = true
        self.slapView.lblJ.addGestureRecognizer(tapJ)
        
        let tapK = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblK))
        self.slapView.lblK.isUserInteractionEnabled = true
        self.slapView.lblK.addGestureRecognizer(tapK)
        
        let tapL = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblL))
        self.slapView.lblL.isUserInteractionEnabled = true
        self.slapView.lblL.addGestureRecognizer(tapL)
        
        let tapM = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblM))
        self.slapView.lblM.isUserInteractionEnabled = true
        self.slapView.lblM.addGestureRecognizer(tapM)
        
        let tapN = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblN))
        self.slapView.lblN.isUserInteractionEnabled = true
        self.slapView.lblN.addGestureRecognizer(tapN)
        
        let tapO = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblO))
        self.slapView.lblO.isUserInteractionEnabled = true
        self.slapView.lblO.addGestureRecognizer(tapO)
        
        let tapP = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblP))
        self.slapView.lblP.isUserInteractionEnabled = true
        self.slapView.lblP.addGestureRecognizer(tapP)
        
        let tapQ = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblQ))
        self.slapView.lblQ.isUserInteractionEnabled = true
        self.slapView.lblQ.addGestureRecognizer(tapQ)
        
        let tapR = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblR))
        self.slapView.lblR.isUserInteractionEnabled = true
        self.slapView.lblR.addGestureRecognizer(tapR)
        
        let tapS = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblS))
        self.slapView.lblS.isUserInteractionEnabled = true
        self.slapView.lblS.addGestureRecognizer(tapS)
        
        let tapT = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblT))
        self.slapView.lblT.isUserInteractionEnabled = true
        self.slapView.lblT.addGestureRecognizer(tapT)
        
        let tapU = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblU))
        self.slapView.lblU.isUserInteractionEnabled = true
        self.slapView.lblU.addGestureRecognizer(tapU)
        
        let tapV = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblV))
        self.slapView.lblV.isUserInteractionEnabled = true
        self.slapView.lblV.addGestureRecognizer(tapV)
        
        let tapW = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblW))
        self.slapView.lblW.isUserInteractionEnabled = true
        self.slapView.lblW.addGestureRecognizer(tapW)
        
        let tapX = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblX))
        self.slapView.lblX.isUserInteractionEnabled = true
        self.slapView.lblX.addGestureRecognizer(tapX)
        
        let tapY = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblY))
        self.slapView.lblY.isUserInteractionEnabled = true
        self.slapView.lblY.addGestureRecognizer(tapY)
        
        let tapZ = UITapGestureRecognizer(target: self, action: #selector(SlapVC.tapLblZ))
        self.slapView.lblZ.isUserInteractionEnabled = true
        self.slapView.lblZ.addGestureRecognizer(tapZ)
        
    }
    
    //MARK:- UITapGestureRecognizer Click
    @objc func tapLblA() {
        print("A")
    }
    
    @objc func tapLblB() {
        print("B")
    }
    
    @objc func tapLblC() {
        print("C")
    }
    
    @objc func tapLblD() {
        print("D")
    }
    
    @objc func tapLblE() {
        print("E")
    }
    
    @objc func tapLblF() {
        print("F")
    }
    
    @objc func tapLblG() {
        print("G")
    }
    
    @objc func tapLblH() {
        print("H")
    }
    
    @objc func tapLblI() {
        print("I")
    }
    
    @objc func tapLblJ() {
        print("J")
    }
    
    @objc func tapLblK() {
        print("K")
    }
    
    @objc func tapLblL() {
        print("L")
    }
    
    @objc func tapLblM() {
        print("M")
    }
    
    @objc func tapLblN() {
        print("N")
    }
    
    @objc func tapLblO() {
        print("O")
    }
    
    @objc func tapLblP() {
        print("P")
    }
    
    @objc func tapLblQ() {
        print("Q")
    }
    
    @objc func tapLblR() {
        print("R")
    }
    
    @objc func tapLblS() {
        print("S")
    }
    
    @objc func tapLblT() {
        print("T")
    }
    
    @objc func tapLblU() {
        print("U")
    }
    
    @objc func tapLblV() {
        print("V")
    }
    
    @objc func tapLblW() {
        print("W")
    }
    
    @objc func tapLblX() {
        print("X")
    }
    
    @objc func tapLblY() {
        print("Y")
    }
    
    @objc func tapLblZ() {
        print("Z")
    }

    //MARK:- API Call
    func getProductList() {
        
        let URL = "http://conflictapi.com/v1/api/productlist?app_id=ac63d082&app_key=0fd23f82553331a53caa5b497708e8b3&allowedpackages=slapbot&activeonly=false&maxrecords=50"
        
        SPAppData.sharedInstance.showLoader()
        SPAPIUtilities.sharedInstance.GetAPICallWith(url: URL) { (response, error) in
            //SPAppData.sharedInstance.dismissLoader()
            if error == nil {
                self.arrProductList.removeAllObjects()
                if let arrResponse = response as? NSArray{
                    for dic in arrResponse {
               //         let model = SlapProductModel(attributeDict: dic as! NSDictionary)
                     //   self.arrProductList.add(model)
                    }
                    
                    DispatchQueue.main.async {
                        self.slapView.collectionSlap.reloadData()
                    }
                    
                }
            } else {
                SPAppData.sharedInstance.showAlert(title: "", message: "Something went wrong", viewController: self)
            }
            self.getSlapDetail()
        }
        
    }
    
    func getSlapDetail() {
        
        let URL = "http://conflictapi.com/v1/api/slap?_dc=1451291462854&app_id=8ff7f067&app_key=177c48931d11028987b854d9fddfa1c3&ActiveOnly=false&maxrecords=50"
        
        SPAPIUtilities.sharedInstance.GetAPICallWith(url: URL) { (response, error) in
            SPAppData.sharedInstance.dismissLoader()
            if error == nil {
                self.arrSlapDetail.removeAllObjects()
                if let arrResponse = response as? NSArray{
                    for dic in arrResponse {
                        let model = SlapDetailModel(attributeDict: dic as! NSDictionary)
                        self.arrSlapDetail.add(model)
                    }
                    
                    DispatchQueue.main.async {
                        self.slapView.tblSlap.reloadData()
                    }
                    
                }
                
            } else {
                SPAppData.sharedInstance.showAlert(title: "", message: "Something went wrong", viewController: self)
            }
            
        }
    }
    
    //MARK:- UITableView DataSource and Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrSlapDetail.count > 0 {
            return self.arrSlapDetail.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "SlapTableCell"
        let cell: SlapTableCell = (tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? SlapTableCell)!
        cell.selectionStyle = .none
        
        if self.arrSlapDetail.count > 0 {
            let model = self.arrSlapDetail[indexPath.row] as? SlapDetailModel
            cell.lblTitle.text = model?.desc
        }
        
        if indexPath.row == 0 {
            cell.imgTblcell.image = UIImage(named: "img_cell1")
        } else if indexPath.row == 1 {
            cell.imgTblcell.image = UIImage(named: "img_cell2")
        } else {
            cell.imgTblcell.image = UIImage(named: "img_cell3")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrSlapDetail.count > 0 {
            return 60
        } else {
            return 0
        }
        
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView,
                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let mehAction = UIContextualAction(style: .normal, title:  "Mah...", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("Meh Action")
            success(true)
        })
        
        mehAction.image = UIImage(named: "ic_new")
        mehAction.backgroundColor = UIColor(red: 52.0/255.0, green: 84.0/255.0, blue: 115.0/255.0, alpha: 1.0)
        
        return UISwipeActionsConfiguration(actions: [mehAction])
        
    }
    
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let favoriteAction = UIContextualAction(style: .normal, title:  "Favorite", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("Favorite action ...")
            success(true)
        })
        
        favoriteAction.backgroundColor = UIColor(red: 231.0/255.0, green: 75.0/255.0, blue: 207.0/255.0, alpha: 1.0)
        favoriteAction.image = UIImage(named: "ic_fav")
        return UISwipeActionsConfiguration(actions: [favoriteAction])
    }
    
    
    
    
    //MARK:- UICollectionView DataSource and Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.arrProductList.count > 0 {
            return self.arrProductList.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let identifier = "SlapCollectionCell"
        let cell: SlapCollectionCell = (collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? SlapCollectionCell)!
        
        if self.arrProductList.count > 0 {
   //         let model: SlapProductModel = self.arrProductList[indexPath.item] as! SlapProductModel
       //     cell.lblSlapTitle.text = model.productName
        }
        
        if indexPath.item == 0 || indexPath.item == 4 || indexPath.item == 6 {
            cell.lblSlapTitle.backgroundColor = UIColor(red: 34.0/255.0, green: 146.0/255.0, blue: 246.0/255.0, alpha: 1.0)
        } else if indexPath.item == 1 {
            cell.lblSlapTitle.backgroundColor = UIColor(red: 249.0/255.0, green: 81.0/255.0, blue: 133.0/255.0, alpha: 1.0)
        } else if indexPath.item == 2 {
            cell.lblSlapTitle.backgroundColor = UIColor(red: 156.0/255.0, green: 101.0/255.0, blue: 211.0/255.0, alpha: 1.0)
        } else if indexPath.item == 3 {
            cell.lblSlapTitle.backgroundColor = UIColor(red: 34.0/255.0, green: 161.0/255.0, blue: 69.0/255.0, alpha: 1.0)
        } else {
            cell.lblSlapTitle.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 1.0)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.arrProductList.count > 0 {
    //        let model = self.arrProductList[indexPath.item] as? SlapProductModel
      //      let title: String? = model?.productName
            let attributes = [NSAttributedStringKey.font: UIFont(name: "PingFangTC-Regular", size: 15.0)]
            let rect: CGRect? = title?.boundingRect(with: CGSize(width: 9999, height: 40), options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            let size = CGSize(width: (rect?.size.width)! + 45, height: 30)
            return size
        } else {
            return CGSize(width: 0, height: 0)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
