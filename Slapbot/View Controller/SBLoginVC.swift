//
//  SBLoginVC.swift
//  Slapbot
//
//  Created by Nikul Dholiya on 27/01/18.
//  Copyright © 2018 Khatri Dhruv. All rights reserved.
//

import UIKit
import Alamofire
import TwitterKit
import FBSDKCoreKit
import FacebookLogin
import FacebookCore
class SBLoginVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnForgetPassword: UIButton!
    @IBOutlet weak var btnLoginIn: UIButton!
    @IBOutlet weak var btnCreateNewAcc: UIButton!
    @IBOutlet weak var btnRemeberMe: UIButton!
    @IBOutlet weak var btnFb: UIButton!
    @IBOutlet weak var btnTwitter: UIButton!
    @IBOutlet weak var viewTwitter: UIView!
    
    @IBOutlet weak var progressView: UIVisualEffectView!
    @IBOutlet weak var progressActiviry: UIActivityIndicatorView!
    
    var isPassword  = false
    
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTwitter.isHidden = true
        
        if let email = UserDefaults.standard.value(forKey: "email") as? String ,let password = UserDefaults.standard.value(forKey: "password") as? String
        {
            progressView.isHidden = false
            progressActiviry.isHidden = false
            progressActiviry.startAnimating()
            login(email: email, password: password)

        }
        else
        {
            progressView.isHidden = true
            progressActiviry.isHidden = true
        }
        
        
        
        
//        let client = TWTRAPIClient.withCurrentUser()
//        client.requestEmail { email, error in
//            if (email != nil) {
//                self.login(email: email!, password: "Q!W@E#R$")
//            } else {
//                print("error: \(error?.localizedDescription)");
//            }
//        }
//
        
//        let logInButton = TWTRLogInButton(logInCompletion: { session, error in
//            if (session != nil) {
//
//                print("signed in as \(session?.userName)");
//            } else {
//                print("error: \(error?.localizedDescription)");
//            }
//        })
        //logInButton.center = self.viewTwitter.center
       //å self.viewTwitter.addSubview(logInButton)
        
       
        
        self.navigationController?.navigationBar.isHidden = true
        SPAppData.sharedInstance.setCornerRadius(view: self.containerView, radius: 3.0)
        SPAppData.sharedInstance.setCornerRadius(view: self.btnLoginIn, radius: 2.0)
        
    }
    
    //MARK:- Button Action
    @IBAction func onTapBtnForgetPassword(_ sender: Any) {
        print("Forget Password")
        let signup = storyboard?.instantiateViewController(withIdentifier: "SBForgotPasswordVC") as! SBForgotPasswordVC
        self.navigationController?.pushViewController(signup, animated: true)
    }
    
    @IBAction func onTapBtnRemeberMe(_ sender: Any) {
        self.btnRemeberMe.isSelected = !self.btnRemeberMe.isSelected
        
        if self.btnRemeberMe.isSelected {
            self.btnRemeberMe.setImage(UIImage(named: "ic_check") , for: UIControlState.normal)
        } else {
            self.btnRemeberMe.setImage(UIImage(named: "ic_uncheck"), for: UIControlState.normal)
        }
        
    }
    
    
    @IBAction func opTapShowPassword(_ sender: UIButton) {
        txtPassword.isSecureTextEntry = isPassword
        isPassword = !isPassword
    }
    
    
    @IBAction func onTapLoginIn(_ sender: Any) {
        
        if(txtUsername.text != "" && txtPassword.text != "")
        {
            progressView.isHidden = false
            progressActiviry.isHidden = false
            progressActiviry.startAnimating()
            login(email: txtUsername.text!, password: txtPassword.text!)
        }
        else
        {
            SPAppData.sharedInstance.showAlert(title: "Whoops", message: "Information is needed in both Username and Password fields.", viewController: self)
        }
        
        
        //IS Login
        /*let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let slapVC = storyboard.instantiateViewController(withIdentifier: "SlapVC") as! SlapVC
        let navigation = UINavigationController(rootViewController: slapVC)
        self.appDelegate.window?.rootViewController = navigation
        self.appDelegate.window?.makeKeyAndVisible()*/
        
    }
    
    func registerWithSocial(email : String, name : String)
    {
        let URL = "http://conflictapi.com/v1/api/account/Register"
        let params = ["Email" : "\(email)",
            "Password" : "Q!W@E#R$",
            "ConfirmPassword" : "Q!W@E#R$",
            "FullName" : "\(name)",
            "UserName" : "\(email)"]
        
        SPAPIUtilities.sharedInstance.postResponseAPI(url: URL, param: params as AnyObject) { (response, error) in
            if error == nil {
                if let arrResponse = response as? NSDictionary{
                        print(arrResponse)
                        if let success = arrResponse["success"] as? Bool
                        {
                        if(success)
                        {
                        UserDefaults.standard.set("\(email)", forKey: "email")
                        UserDefaults.standard.set("Q!W@E#R$", forKey: "password")
                        let keyboardEnableVC = self.storyboard?.instantiateViewController(withIdentifier: "SBKeyboardSettingVC") as! SBKeyboardSettingVC
                        self.navigationController?.pushViewController(keyboardEnableVC, animated: true)
                        }
                        else
                        {
                            self.login(email: email, password: "Q!W@E#R$")
//                            self.progressView.isHidden = true
//                            self.progressActiviry.isHidden = true
//                            if let arr = arrResponse["message"] as? NSArray
//                            {
//                                SPAppData.sharedInstance.showAlert(title: "Whoops", message: "\(arr.firstObject!)", viewController: self)
//                            }
//                            else
//                            {
//                                SPAppData.sharedInstance.showAlert(title: "Whoops", message: "Unknown Error Occured", viewController: self)
//                            }
                        }
                    }
                    else
                        {
                            self.progressView.isHidden = true
                            self.progressActiviry.isHidden = true
                            SPAppData.sharedInstance.showAlert(title: "Whoops", message: "\((error?.localizedDescription)!)", viewController: self)
                    }
                }
            } else {
                self.progressView.isHidden = true
                self.progressActiviry.isHidden = true
                SPAppData.sharedInstance.showAlert(title: "Whoops", message: "\((error?.localizedDescription)!)", viewController: self)
            }
        }
    }
    
    
    func login(email : String, password : String)
    {
       // /Login
        
        let URL = "http://conflictapi.com/v1/api/account/Login"
        let params = ["UserName" : "\(email)",
                    "Password" : "\(password)"]

        SPAPIUtilities.sharedInstance.postResponseAPI(url: URL, param: params as AnyObject) { (response, error) in
            if error == nil {
                if let arrResponse = response as? NSDictionary{
                    print(arrResponse)
                    if let success = arrResponse["success"] as? Bool
                    {
                        if(success)
                        {
                            if(self.btnRemeberMe.isSelected)
                            {
                        UserDefaults.standard.set("\(email)", forKey: "email")
                        UserDefaults.standard.set("\(password)", forKey: "password")
                            }
                        if let settings = UserDefaults.standard.value(forKey: "settings") as? String
                        {
                            let keyboardEnableVC = self.storyboard?.instantiateViewController(withIdentifier: "SBPurchaseVC") as! SBPurchaseVC
                            self.navigationController?.pushViewController(keyboardEnableVC, animated: true)
                        }
                        else
                        {
                        let keyboardEnableVC = self.storyboard?.instantiateViewController(withIdentifier: "SBKeyboardSettingVC") as! SBKeyboardSettingVC
                        self.navigationController?.pushViewController(keyboardEnableVC, animated: true)
                        }
                        }
                        else
                        {
                            self.progressView.isHidden = true
                            self.progressActiviry.isHidden = true
                            if let arr = arrResponse["message"] as? NSArray
                            {
                                SPAppData.sharedInstance.showAlert(title: "Whoops", message: "\(arr.firstObject!)", viewController: self)
                            }
                            else
                            {
                                SPAppData.sharedInstance.showAlert(title: "Whoops", message: "Unknown Error Occured", viewController: self)
                            }
                        }
                    }
                    else
                    {
                        if let token = arrResponse["access_token"] as? String
                        {
                            if(self.btnRemeberMe.isSelected)
                        {
                        UserDefaults.standard.set("\(email)", forKey: "email")
                        UserDefaults.standard.set("\(password)", forKey: "password")
                            }
                        if let settings = UserDefaults.standard.value(forKey: "settings") as? String
                        {
                            let keyboardEnableVC = self.storyboard?.instantiateViewController(withIdentifier: "SBPurchaseVC") as! SBPurchaseVC
                            self.navigationController?.pushViewController(keyboardEnableVC, animated: true)
                        }
                        else
                        {

                        let keyboardEnableVC = self.storyboard?.instantiateViewController(withIdentifier: "SBKeyboardSettingVC") as! SBKeyboardSettingVC
                        self.navigationController?.pushViewController(keyboardEnableVC, animated: true)
                        }
                        }
                        else
                        {
                            self.progressView.isHidden = true
                            self.progressActiviry.isHidden = true
                            SPAppData.sharedInstance.showAlert(title: "Whoops", message: "\(arrResponse["error_description"] as? String ?? "Error Occured")", viewController: self)
                        }
                    }
                }
            } else {
                self.progressView.isHidden = true
                self.progressActiviry.isHidden = true
                SPAppData.sharedInstance.showAlert(title: "Whoops", message: "\((error?.localizedDescription)!)", viewController: self)
            }
        }
    }
    
    
    func getFBUserData(){
    
        
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    if let dic = result as? NSDictionary
                    {
                        self.registerWithSocial(email: "\(dic["email"] as? String ?? "")", name: "\(dic["name"] as? String ?? "")")
                    }
                    print(result!)
                    
                }
            })
        }
    }
    
    
    @IBAction func onTapBtnFb(_ sender: Any) {
        print("Facebook")
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions:[.publicProfile, .email], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                self.getFBUserData()
            }
        }
    }
    
    
    
    @IBAction func onTapBtnTwitter(_ sender: Any) {
        print("Twitter")
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                let alertController = UIAlertController(title: "Email", message: "Enter your email ID", preferredStyle: .alert)
                
                let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
                    alert -> Void in
                    
                    let time = alertController.textFields![0] as UITextField
                    
                    if time.text != "" {
                        self.registerWithSocial(email: "\(time.text!)", name: "\((session?.userName)!)")
                    }else{
                        // Show Alert Message to User As per you want
                    }
                    
                })
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                    (action : UIAlertAction!) -> Void in
                    
                })
                
                
                alertController.addTextField { (textField : UITextField!) -> Void in
                    textField.placeholder = "Email for Register"
                }
                
                alertController.addAction(saveAction)
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                
                
             //   print("signed in as \(session?.userName)");
            } else {
                print("error: \(error?.localizedDescription as? String ?? "Error Occured")");
            }
        })
    }
    
    @IBAction func onTapBtnCreateNewAcc(_ sender: Any) {
        print("Create New Account")
        let signup = storyboard?.instantiateViewController(withIdentifier: "SBSignUpVC") as! SBSignUpVC
        self.navigationController?.pushViewController(signup, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}
