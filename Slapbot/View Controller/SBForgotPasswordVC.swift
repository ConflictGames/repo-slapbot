//
//  SBForgotPasswordVC.swift
//  Slapbot
//
//  Created by dhruv Khatri on 17/02/18.
//  Copyright © 2018 Khatri Dhruv. All rights reserved.
//

import UIKit
import Alamofire
class SBForgotPasswordVC: UIViewController {

    @IBOutlet weak var activityProgress: UIActivityIndicatorView!
    @IBOutlet weak var txtEmail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityProgress.startAnimating()
        activityProgress.isHidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        activityProgress.isHidden = false
        forgot(email: "\(txtEmail.text!)")
    }
    
    func forgot(email : String)
    {
        // /Login
        
      
        let params : Parameters = ["Username" : "\(email)"]
        
        Alamofire.request("http://conflictapi.com/v1/api/Account/PasswordRestToken", method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                print(response)
                switch response.result {
                case .success(let data):
                    print(data)
                    let alertController = UIAlertController(title: "Password", message: "Enter your new password & Token", preferredStyle: .alert)
                    
                                            let saveAction = UIAlertAction(title: "Update", style: .default, handler: {
                                                alert -> Void in
                    
                                                let time = alertController.textFields![0] as UITextField
                                                let token = alertController.textFields![1] as UITextField
                    
                                                if time.text != "" && token.text != "" {
                                                    self.changePass(token: "\(token.text!)", email: email, password: "\(time.text!)", confom: "\(time.text!)")
                                                }else{
                                                    // Show Alert Message to User As per you want
                                                }
                    
                                            })
                    
                                            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                                                (action : UIAlertAction!) -> Void in
                    
                                            })
                    
                    
                                            alertController.addTextField { (textField : UITextField!) -> Void in
                                                textField.placeholder = "New Password"
                                            }
                                            alertController.addTextField { (textField : UITextField!) -> Void in
                                            textField.placeholder = "Token"
                                            }
                                            alertController.addAction(saveAction)
                                            alertController.addAction(cancelAction)
                    
                                            self.present(alertController, animated: true, completion: nil)
                    
                    
                    ////                        let keyboardEnableVC = self.storyboard?.instantiateViewController(withIdentifier: "SBLoginVC") as! SBLoginVC
                    ////                        self.navigationController?.pushViewController(keyboardEnableVC, animated: true)
                    break
                case .failure(let error):
                    self.activityProgress.isHidden = true
                    SPAppData.sharedInstance.showAlert(title: "Whoops", message: "Invalid User", viewController: self)
                    break
                }
        }
        
        
//        SPAPIUtilities.sharedInstance.postResponseAPI(url: URL, param: params as AnyObject) { (response, error) in
//            //if error == nil {
//                if let arrResponse = response as? NSDictionary{
//
//                        let alertController = UIAlertController(title: "Password", message: "Enter your new password & Token", preferredStyle: .alert)
//
//                        let saveAction = UIAlertAction(title: "Update", style: .default, handler: {
//                            alert -> Void in
//
//                            let time = alertController.textFields![0] as UITextField
//                            let token = alertController.textFields![1] as UITextField
//
//                            if time.text != "" && token.text != "" {
//                                self.changePass(token: "\(token.text!)", email: email, password: "\(time.text!)", confom: "\(time.text!)")
//                            }else{
//                                // Show Alert Message to User As per you want
//                            }
//
//                        })
//
//                        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
//                            (action : UIAlertAction!) -> Void in
//
//                        })
//
//
//                        alertController.addTextField { (textField : UITextField!) -> Void in
//                            textField.placeholder = "New Password"
//                        }
//                        alertController.addTextField { (textField : UITextField!) -> Void in
//                        textField.placeholder = "Token"
//                        }
//                        alertController.addAction(saveAction)
//                        alertController.addAction(cancelAction)
//
//                        self.present(alertController, animated: true, completion: nil)
//
//
////                        let keyboardEnableVC = self.storyboard?.instantiateViewController(withIdentifier: "SBLoginVC") as! SBLoginVC
////                        self.navigationController?.pushViewController(keyboardEnableVC, animated: true)
//                }
//           // } else {
//          //      self.activityProgress.isHidden = true
//          //      SPAppData.sharedInstance.showAlert(title: "Whoops", message: "\((error?.localizedDescription)!)", viewController: self)
//         //   }
//        }
    }

    func changePass(token : String, email : String, password : String, confom : String)
    {
        let URL = "http://conflictapi.com/v1/api/account/UpdatePassword"
        let params = ["userName" : "\(email)",
            "resetToken": "\(token)",
            "newPassword": "\(password)",
            "confirmPassword": "\(confom)"]
        SPAPIUtilities.sharedInstance.postResponseAPI(url: URL, param: params as AnyObject) { (response, error) in
            if error == nil {
                if let arrResponse = response as? NSDictionary{
                        print(arrResponse)
                        if let value = arrResponse["success"] as? Bool
                        {
                        let alert = UIAlertController(title: "Whoops", message: "Successfully Changed", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { UIAlertAction in
                            let keyboardEnableVC = self.storyboard?.instantiateViewController(withIdentifier: "SBLoginVC") as! SBLoginVC
                            self.navigationController?.pushViewController(keyboardEnableVC, animated: true)
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                        }
                        
                        else if let value = arrResponse["modelState"] as? NSDictionary
                        {
                            let dic = value as! NSDictionary
                            //let subDic = dic as! NSDictionary
                            let arr = dic["errorMessage"] as! NSArray
                            let alert = UIAlertController(title: "Whoops", message: "\(arr[0] as? String ?? "Unknown Error")", preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { UIAlertAction in
                                self.activityProgress.isHidden = true

                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            self.activityProgress.isHidden = true
                            SPAppData.sharedInstance.showAlert(title: "Whoops", message: "Unknown Error", viewController: self)
                        }
                    
                }
            } else {
                self.activityProgress.isHidden = true
                SPAppData.sharedInstance.showAlert(title: "Whoops", message: "Username Not Exist", viewController: self)
            }
        }
    }
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        let loginVC = storyboard?.instantiateViewController(withIdentifier: "SBLoginVC") as! SBLoginVC
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension String {
    
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
}
