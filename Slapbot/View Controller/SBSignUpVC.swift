//
//  SBSignUpVC.swift
//  Slapbot
//
//  Created by dhruv Khatri on 17/02/18.
//  Copyright © 2018 Khatri Dhruv. All rights reserved.
//

import UIKit

class SBSignUpVC: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConformPassword: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var progressView: UIVisualEffectView!
    
    @IBOutlet weak var activityProgress: UIActivityIndicatorView!
    
    var isPassword = false
    var isConformPassword = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        progressView.isHidden = true
        activityProgress.isHidden = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Register(email : String, password : String, confromPassword : String, fullName : String, userName : String)
    {
        // /Login
        
//        Email:dhruv@gmail.com
//        Password:dhruv@123
//        ConfirmPassword:dhruv@123
//        FullName:Dhruv Khatri
//        UserName:dhruv143
        
        let URL = "http://conflictapi.com/v1/api/account/Register"
        let params = ["Email" : "\(email)",
            "Password" : "\(password)",
            "ConfirmPassword" : "\(confromPassword)",
            "FullName" : "\(fullName)",
            "UserName" : "\(userName)"]
        
        SPAPIUtilities.sharedInstance.postResponseAPI(url: URL, param: params as AnyObject) { (response, error) in
            if error == nil {
                if let arrResponse = response as? NSDictionary{
                    if let success = arrResponse["success"] as? Bool
                    {
                        if(success)
                        {
                        print(arrResponse)
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let keyboardEnableVC = storyboard.instantiateViewController(withIdentifier: "SBKeyboardSettingVC") as! SBKeyboardSettingVC
                        self.navigationController?.pushViewController(keyboardEnableVC, animated: true)
                        }
                        else
                        {
                            self.progressView.isHidden = true
                            self.activityProgress.isHidden = true
                            if let arr = arrResponse["message"] as? NSArray
                            {
                            SPAppData.sharedInstance.showAlert(title: "Whoops", message: "\(arr.firstObject!)", viewController: self)
                            }
                            else
                            {
                                SPAppData.sharedInstance.showAlert(title: "Whoops", message: "Unknown Error Occured", viewController: self)
                            }
                        }
                    }
                    else
                    {
                        if let access_token = arrResponse["access_token"] as? String
                        {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let keyboardEnableVC = storyboard.instantiateViewController(withIdentifier: "SBKeyboardSettingVC") as! SBKeyboardSettingVC
                            self.navigationController?.pushViewController(keyboardEnableVC, animated: true)
                        }
                        else
                        {
                        self.progressView.isHidden = true
                        self.activityProgress.isHidden = true
                        SPAppData.sharedInstance.showAlert(title: "Whoops", message: "\(arrResponse["error_description"] as? String ?? "")", viewController: self)
                        }
                    }
                }
                else
                {
                    self.progressView.isHidden = true
                    self.activityProgress.isHidden = true
                    SPAppData.sharedInstance.showAlert(title: "Whoops", message: "Missing details, please fill in all fields", viewController: self)
                }
                }
                else {
                self.progressView.isHidden = true
                self.activityProgress.isHidden = true
                SPAppData.sharedInstance.showAlert(title: "Whoops", message: "Unknown Error Occured", viewController: self)
            }
        }
    }
    
    @IBAction func btnSignUpClicked(_ sender: UIButton) {
        if(txtEmail.text != "" && txtPassword.text != "" && txtConformPassword.text != "" && txtUserName.text != "")
        {
        progressView.isHidden = false
        activityProgress.isHidden = false
        activityProgress.startAnimating()
        Register(email: txtEmail.text!, password: txtPassword.text!, confromPassword: txtConformPassword.text!, fullName: txtUserName.text!, userName: txtUserName.text!)
        }
        else
        {
            SPAppData.sharedInstance.showAlert(title: "Whoops", message: "Missing details, please fill in all fields", viewController: self)
        }
    }
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        let loginVC = storyboard?.instantiateViewController(withIdentifier: "SBLoginVC") as! SBLoginVC
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func onTapShowPassword(_ sender: UIButton) {
        txtPassword.isSecureTextEntry = isPassword
        isPassword = !isPassword
    }
    
    @IBAction func onTapShowConfomPassword(_ sender: UIButton) {
        txtConformPassword.isSecureTextEntry = isConformPassword
        isConformPassword = !isConformPassword
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
